import { UserService } from './services/user.service';
import { Component } from '@angular/core';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { EventsService } from './services/events.service';
import { UtilityService } from './services/utility.service';
import { PermissionsService } from './services/permissions.service';
import { AudioService } from './services/audio.service';
import { NetworkService } from './services/network.service';
import { NavService } from './services/nav.service';
import { ActivatedRoute, Router } from '@angular/router';

import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { SqliteService } from './services/sqlite.service';
import { FirebaseService } from './services/firebase.service';
import { FormatPhoneService } from './services/format-phone.service';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isEmailVerificationPending = false;
  user_role_id = -1;
  isModalOpen;

  pages: any[] = [
    {
      title: 'My Account',
      component: 'RegistrationPage',
      params: { user_id: null, hidebackbtn: false },
    },
    { title: 'Logout', method: 'logout' },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menuCtrl: MenuController,
    private events: EventsService,
    public userService: UserService,
    public utilityProvider: UtilityService,
    public users: UserService,
    public audio: AudioService,
    public network: NetworkService,
    public nav: NavService,
    public activateRoute: ActivatedRoute,
    private router: Router,
    private modalController: ModalController,
    private sqlite: SqliteService,
    private firebaseService: FirebaseService,
    public fphs: FormatPhoneService
  ) {
    platform.ready().then( async () => {
      menuCtrl.enable(false, 'authenticated');
      statusBar.styleDefault();

      await this.setupPhoneCodesInService();
      if (
        platform.is('cordova') ||
        platform.is('ios') ||
        platform.is('android')
      ) {
        this.initializeApp();
      }

      // set default url from app side
      this.sqlite
        .initialize()
        .then(() => {
          this.network.getBaseUrl().then((res) => {
            console.log(res);
            localStorage.setItem('baseurl', res);
            this.beInitialize();
            this.nav.push('pages/splash');
          });
        })
        .catch((err) => alert(err));
    });
  }
  setupPhoneCodesInService(){

    return new Promise( async resolve => {


      const res = await this.network.getAvailableDialCodes();
      this.fphs.availableDialCodes = res;


      resolve(true)
    });

  }


  beInitialize() {
    // assign events
    this.assignEvents();

    this.platform.backButton.subscribeWithPriority(1, () => {
      console.log('hit back btn');
    });

    // this.network.getAppointedLatLongsForGuard().then(async (data) => {
      // if (data.is_guard_geo_fence && data.list.length == 0) {
      //   await this.utilityProvider.showAlert(
      //     'You have no Location appointed',
      //     'Error'
      //   );
      //   this.exitApp();
      // }
    // });

    this.router.events.subscribe(async () => {
      // if (router.url.toString() === "/tabs/home" && isModalOpened) this.modalController.dismiss();
    });

    document.addEventListener(
      'backbutton',
      (event) => {
        event.preventDefault();
        event.stopPropagation();
        const url = this.router.url;
        console.log(url);
        this.createBackRoutingLogics(url);
      },
      false
    );
  }

  async createBackRoutingLogics(url) {
    if (
      url.includes('login') ||
      url.includes('signup') ||
      url.includes('dashboard')
    ) {
      const isModalOpen = await this.modalController.getTop();
      console.log(isModalOpen);
      if (isModalOpen) {
        this.modalController.dismiss({ data: 'A' });
      } else {
        this.exitApp();
      }
    } else {
      if (this.isModalOpen) {
      }
    }
  }

  exitApp() {
    navigator['app'].exitApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.audio.preload();

      if(Capacitor.getPlatform() != 'web'){
        this.setupFMC();
      }

    });
  }

  setupFMC() {
    PushNotifications.requestPermissions().then((result) => {
      if (result.receive === 'granted') {
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration', (token: Token) => {
      localStorage.setItem('fcm_token', token.value);
      this.events.publish('user:settokentoserver');
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      console.error('Error on registration: ' + JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        console.log('Push received: ' + JSON.stringify(notification));
        this.audio.play('');
        this.events.publish('dashboard:notificationReceived');
        this.events.publish('dashboard:refreshpage');
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        console.log('Push action performed: ' + JSON.stringify(notification));
        this.events.publish('dashboard:refreshpage');
      }
    );
  }

  assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
    this.events.subscribe('user:process', this.processUserData.bind(this));
    this.events.subscribe('user:successpage', this.setSuccessPage.bind(this));
    this.events.subscribe(
      'user:settokentoserver',
      this.firebaseService.setTokenToServer.bind(this)
    );
    this.events.subscribe(
      'user:shownotificationalert',
      this.notificationReceivedalert.bind(this)
    );
    // this.events.subscribe('user:setcontactstodatabase', this.setContacts.bind(this));
  }

  login(user) {
    console.log(user);

    const showelcome = user.showelcome;
    const email_data = {
      email: user.email,
      password: user.password,
    };

    const phone_data = {
      phone_number: user.phone_number,
      password: user.password,
      register_with_phonenumber: true,
    };

    const data =
      user.register_with_phonenumber == true ? phone_data : email_data;

    this.network.login(data).then(
      async (res) => {
        const user = res.user;
        const isGuard = user.role_id;
        if (isGuard != 8) {
          this.utilityProvider.presentToast(
            'Only Guard / Security Personnal can login'
          );
          this.nav.setRoot('LoginPage');
          return;
        }

        this.menuCtrl.enable(true, 'authenticated');
        const token = res.success.token;

        user.token = token;
        user.active = 1;

        await this.processUserData(user, showelcome);
      },
      (err) => {}
    );
  }

  logout() {
    this.menuCtrl.enable(false, 'authenticated');
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.sqlite.setLogout()
    this.nav.setRoot('pages/LoginPage');
  }

  getUser() {
    this.network.getUser().then(
      async (res: any) => {
        console.log(res);
        const tuser = res.user;
        if (tuser) {
          this.processUserData(tuser, false);
        } else {
          // redirect to steps
          this.nav.push('Error404Page', {
            afterLogin: true,
            animate: true,
            direction: 'forward',
          });
        }
      },
      (err) => {
        this.logout();
      }
    );
  }

  async processUserData(user, showelcome = false) {
    // check if sqlite set already, if not fetch records
    // const user = user;

    const fcm_token = await this.firebaseService.getFCMToken();
    user.fcm_token = fcm_token;
    this.user_role_id = parseInt(user.role_id);
    this.utilityProvider.setKey('user_role_id', this.user_role_id);
    // localStorage.setItem('user', JSON.stringify(user));
    if(user.token){
      localStorage.setItem('token', user.token);
    }

    await this.sqlite.setUserInDatabase(user);
    const saveduser = user;
    this.menuCtrl.enable(true, 'authenticated');

    if (!saveduser) {
      this.logout();
      return;
    }

    this.userService.setUser(saveduser);

    const currentUrl = this.nav.router.url;
    console.log(currentUrl);

    if (currentUrl == '/pages/dashboard') {
      this.events.publish('dashboard:initialize');
    } else {
      this.nav.setRoot('pages/dashboard', {
        showelcome,
        animate: true,
        direction: 'forward',
      });
    }
  }

  // private async setContacts(user){
  //   this.users.getContactDatabseOfUser(user).then( v => {
  //     console.log("import completed", v);
  //   });
  // }

  setSuccessPage(params) {
    //// console.log(params);
    this.menuCtrl.enable(true, 'authenticated');
    this.nav.setRoot('SuccessPage', params);
  }

  notificationReceivedalert(data) {
    // // console.log(data.showalert);
    if (data.hasOwnProperty('showalert')) {
      // this.rnotif = true;
      this.utilityProvider.showAlert(data.showalert).then(() => {
        if (data.showalert == 'Pass scanned successfully') {
          this.nav.setRoot('DashboardPage', {
            animate: true,
            direction: 'backword',
          });
        }
      });
    }
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuCtrl.close();
    if (page.component == 'RegistrationPage') {
      this.events.publish('dashboard:updateprofile');
      return;
    }

    if (page.hasOwnProperty('component')) {
      this.nav.push(page.component, page.params);
      return;
    }
    if (page.hasOwnProperty('method')) {
      this[page.method]();
    }
  }
}
