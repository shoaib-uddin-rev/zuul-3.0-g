import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FCM } from '@ionic-native/fcm/ngx';
import { NgxPubSubModule, NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
// import { Crop } from '@ionic-native/crop/ngx';
// import { Contacts} from '@ionic-native/contacts/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
// import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { StorageService } from './services/basic/storage.service';
import { ApiService } from './services/api.service';
import { NetworkService } from './services/network.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PagesModule } from './pages/pages.module';
import { EventsService } from './services/events.service';
import { InterceptorService } from './services/interceptor.service';
import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
// import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FirebaseService } from './services/firebase.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { AngularFireModule } from '@angular/fire/compat';
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
} from '@angular/fire/compat/database';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { Printer } from '@awesome-cordova-plugins/printer/ngx';
import { SqliteService } from './services/sqlite.service';
import { SQLite, SQLiteObject } from '@awesome-cordova-plugins/sqlite/ngx';
// import { File } from '@awesome-cordova-plugins/file/ngx';

const firebaseConfig = {
  apiKey: 'AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA',
  authDomain: 'zuul-master.firebaseapp.com',
  databaseURL: 'https://zuul-master.firebaseio.com',
  projectId: 'zuul-master',
  storageBucket: 'zuul-master.appspot.com',
  messagingSenderId: '1013551636023',
  appId: '1:1013551636023:web:55e1b9e9286170e003e9b2',
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios',
    }),
    AppRoutingModule,
    HttpClientModule,
    PagesModule,
    NgxPubSubModule,
    NgxQRCodeModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    SQLite,
    FCM,
    DatePicker,
    Base64,
    Camera,
    AngularFireDatabase,
    // Crop,
    // Contacts,
    Geolocation,
    NativeGeocoder,
    Calendar,
    LaunchNavigator,
    CallNumber,
    InAppBrowser,
    Keyboard,
    OpenNativeSettings,
    // AndroidPermissions,
    NativeStorage,
    BarcodeScanner,
    // custom providers
    NgxPubSubService,
    EventsService,
    FirebaseService,
    StorageService,
    ApiService,
    SqliteService,
    NetworkService,
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    Printer,
    // File,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
