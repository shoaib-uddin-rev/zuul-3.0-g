import { Injectable } from '@angular/core';
import { Printer, PrintOptions } from '@awesome-cordova-plugins/printer/ngx';

@Injectable({
  providedIn: 'root',
})
export class PrinterService {
  constructor(private printer: Printer) {}

  print(content) {
    return new Promise(async (resolve) => {
      const res = await this.printer.isAvailable();

      if (res) {
        console.log(res);

        let options: PrintOptions = {
          name: 'MyDocument',
          duplex: true,
          orientation: 'landscape',
          monochrome: true,
        };

        this.printer
          .print(content, options)
          .then(
            (res) => {
              console.log(res);
              resolve(res);
              // resolve(res)
            },
            (err) => {
              console.log(err);
              resolve(false);
            }
          )
          .catch((e) => {
            console.log(e);
            resolve(false);
          });

        // if (printer_res) {
        // } else {
        //   resolve(false);
        // }
      } else {
        resolve(false);
      }
    });
  }
}
