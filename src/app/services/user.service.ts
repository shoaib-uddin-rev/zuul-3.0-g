/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { Config } from '../config/main.config';
import { EventsService } from './events.service';
import { FirebaseService } from './firebase.service';
import { NavService } from './nav.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
const sampleUser = require('./../data/user.json');

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // endpoint: string = Config.api + "/user";
  _user: any;
  avatar = sampleUser.avatar;
  switchUserAccount: any;

  constructor(
    public utilityProvider: UtilityService,
    public events: EventsService,
    public network: NetworkService,
    public nav: NavService,
    public sqlite: SqliteService,
    public firebaseService: FirebaseService
  ) {}

  assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    // this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
    // this.events.subscribe('user:successpage', this.setSuccessPage.bind(this));
    // this.events.subscribe('user:shownotificationalert', this.notificationReceivedalert.bind(this));
  }

  // notificationReceivedalert(data) {
  //   // // console.log(data.showalert);
  //   if (data.hasOwnProperty('showalert')) {
  //     // this.rnotif = true;
  //     this.utilityProvider.showAlert(data.showalert).then(() => {
  //       if (data.showalert == 'Pass scanned successfully') {
  //         this.nav.setRoot('DashboardPage', {
  //           animate: true,
  //           direction: 'backword'
  //         });
  //       }
  //     });
  //   }
  // }

  // setSuccessPage(params) {
  //   //// console.log(params);
  //   // this.menuCtrl.enable(true, 'authenticated');
  //   this.nav.setRoot('SuccessPage', params);
  // }

  login(user) {
    console.log(user);

    const phone_data = {
      dial_code: user.dial_code,
      phone_number: user.phone_number,
      password: user.password,
      register_with_phonenumber: true,
    };

    this.network.login(phone_data).then(
      async (res) => {
        console.log(res);
        const uuser = res.user;
        const roleId = parseInt(uuser.role_id, 10);
        if (roleId != 8 && roleId != 11) {
          this.utilityProvider.presentToast(
            'Only Guard / Security Personnal can login here'
          );
          this.logout();
          // this.nav.setRoot('pages/login');
          return;
        }

        // this.menuCtrl.enable(true, 'authenticated');
        const token = res.success.token;
        uuser.token = token;
        uuser.active = 1;
        await this.processUserData(uuser, false);
      },
      (err) => {}
    );
  }

  logout(sw = null) {
    // this.menuCtrl.enable(false, 'authenticated');
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.sqlite.setLogout();

    if (!sw) {
      this.nav.setRoot('pages/login');
    } else {
      this.nav.setRoot('pages/login', {
        switchUser: true,
        phone_number: sw['phone_number'],
      });
    }
  }

  public getActiveUser(): any {
    let user = localStorage.getItem('user');
    user = user ? JSON.parse(user) : null;
    return user;
  }

  getAdditionalContact(contact_id) {
    console.log(contact_id);

    this.network.getAdditionalPhone(contact_id).then(
      async (res) => {
        console.log(res);
        // const uuser = res.user;
        // const roleId = parseInt(uuser.role_id, 10);
        // if (roleId != 8 && roleId != 11) {
        //   this.utilityProvider.presentToast(
        //     'Only Guard / Security Personnal can login here'
        //   );
        //   this.logout();
        //   // this.nav.setRoot('pages/login');
        //   return;
        // }

        // // this.menuCtrl.enable(true, 'authenticated');
        // const token = res.success.token;
        // uuser.token = token;
        // uuser.active = 1;
        // await this.processUserData(uuser, false);
      },
      (err) => {}
    );
  }

  public getUser() {
    return new Promise(async (resolve) => {
      this.network.getUser().then(
        async (user: any) => {
          console.log('peel', user);
          user = user.user;
          if (user) {
            this.processUserData(user, false);
            resolve(user);
          } else {
            // redirect to steps
            this.logout();
          }
        },
        (err) => {
          this.logout();
        }
      );
    });
  }

  async setTokenToServer() {
    this.firebaseService.setTokenToServer();
  }

  async processUserData(user, showelcome) {
    // check if sqlite set already, if not fetch records
    // const _user = user
    console.log(user);

    user.fcm_token = await this.firebaseService.getFCMToken();
    // this.user_role_id = parseInt(_user['role_id']);
    // this.utilityProvider.setKey('user_role_id', this.user_role_id);
    const saveduser = user;
    await this.sqlite.setUserInDatabase(user);
    localStorage.setItem('user', JSON.stringify(saveduser));
    localStorage.setItem('token', user.token);
    // this.menuCtrl.enable(true, 'authenticated');
    console.log(saveduser);

    if (!saveduser) {
      this.logout();
      return;
    }

    this.setUser(saveduser);
    this.nav.setRoot('pages/dashboard');
    // this.canBeResident = (parseInt(saveduser["can_user_become_resident"]) == 1);
    // this.canShowSettings = parseInt(saveduser["role_id"]) != 7

    // let currentUrl = this.nav.router.url;
    // console.log(currentUrl);

    // if (currentUrl == '/1/DashboardPage') {
    //   this.events.publish('dashboard:initialize');
    // } else {
    //   this.nav.setRoot('1/DashboardPage',
    //     {
    //       showelcome: showelcome,
    //       animate: true,
    //       direction: 'forward'
    //     }
    //   );
    // }
  }

  setUser(user) {
    this._user = user;
  }

  getCurrentUser() {
    return this._user;
  }

  update(data, token) {
    return {
      data,
      token,
    };
  }
}
