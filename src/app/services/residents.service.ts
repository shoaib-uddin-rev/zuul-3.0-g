import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root',
})
export class ResidentsService {
  allResidents: any[] = [];
  offset = 0;

  constructor(private sqlite: SqliteService) {}

  async getAllResidents(search, offset) {
    const residents: any = await this.sqlite.getAllResidents(
      search,
      offset,
      true
    );

    if (this.offset == 0) {
      this.allResidents = residents.residents_list;
    } else {
      this.allResidents = [...this.allResidents, ...residents.residents_list];
    }
    this.offset = residents.offset;
  }
  async fakeDncRecords() {
    return new Promise(async (resolve) => {
      const userId = await this.sqlite.getActiveUserId();
      let records = [];
      for (let index = 0; index < 100; index++) {
        let obj = {
          id: index + 1,
          vendor_name: this.makeid(10),
          is_private_household: true,
          vendor_contacts: this.makeNumberid(10),
          user_id: userId,
        };
        records.push(obj);
      }
      resolve(records);
    });
  }

  makeid(length) {
    var result = '';
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  makeNumberid(length) {
    var result = '';
    var characters = '0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
