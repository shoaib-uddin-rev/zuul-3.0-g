import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AsYouType, CountryCode } from 'libphonenumber-js';
import { ContactsService } from './contacts.service';

@Injectable({
  providedIn: 'root',
})
export class FormatPhoneService {

  audio: any;
  received = false;
  availableDialCodes = [];

  constructor (public platform: Platform ) {
    // console.log('Hello AudioProvider Provider');
  }

  async initialize() {
    // const res = await this.network.getAvailableDialCodes();
    // console.log(res);
    // this.availableDialCodes = res;
  }

  formatPhoneByDialCode(phonenumber: string, dial_code) {
    // console.log(phonenumber, dial_code, this.availableDialCodes);
    const cleaned = ('' + dial_code).replace(/\D/g, '');
    console.log('cleaned', cleaned);

    let findDialCOde = this.availableDialCodes.find(
      (x) => x.dial_code == cleaned
    );
    console.log('findDialCOde', findDialCOde);
    console.log('phonenumber', phonenumber);

    // if (findDialCOde) {
    //   return this.formatPhone(phonenumber, findDialCOde.country_code);
    // }

    return phonenumber;
  }

  formatPhone(phonenumber: string, country_code): string {
    const cc: CountryCode = country_code;
    let c = '';
    // if dial_code shared is available in available dialcodes
    console.log(this.availableDialCodes, country_code);
    let findDialCOde = this.availableDialCodes.find(
      (x) => x.country_code == country_code
    );

    // let country_code: CountryCode = 'US'
    if (findDialCOde) {
      c = phonenumber.substring(0, findDialCOde.digit);
    }
    //   dial_code = '+1';

    // }

    // country_code = findDialCOde.country_code;
    console.log(c);
    // const pn = parsePhoneNumber(c, { regionCode: country_code });

    // console.log(pn, cc);

    // let p = pn?.number?.national ? pn?.number?.national : c;
    if (country_code == 'HN') {
      let r = this.eightDigitformatPhoneNumber(c);
      console.log(r);
      return r;
    }else{
      let c =  this.onkeyupFormatPhoneNumberRuntime(phonenumber, false, 'US')
       return c
    }

    // return pn?.number?.national ? pn?.number?.national : c;
  }

  // 8 digit format
  eightDigitformatPhoneNumber(phoneNumber) {
    function numDigits(x: number) {
      return (Math.log(x) * Math.LOG10E + 1) | 0;
    }

    const cleaned = ('' + phoneNumber).replace(/\D/g, '');

    // only keep number and +
    const p1 = cleaned.match(/\d+/g);
    if (p1 == null) {
      return cleaned;
    }
    const p2 = phoneNumber.match(/\d+/g).map(Number);
    const len = numDigits(p2);
    // document.write(len + " " );
    switch (len) {
      case 1:
      case 2:
      case 3:
      case 4:
        return '' + phoneNumber + '';
      case 5:
      case 6:
      case 7:
      case 8:
        var f1 = '' + phoneNumber.toString().substring(0, 4) + '';
        var f2 = phoneNumber.toString().substring(len, 4);
        return f1 + '-' + f2;
      default:
        var f1 = '' + phoneNumber.toString().substring(0, 4) + '';
        var f2 = phoneNumber.toString().substring(len, 4);
        return f1 + '-' + f2;
    }
  }

  onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true, dial_code = 'US') {

    if (phoneNumber == null || phoneNumber == '') {
      return phoneNumber;
    }

    phoneNumber = this.getOnlyDigits(phoneNumber);
    ``



    // phoneNumber = phoneNumber.substring(phoneNumber.length - 1,-11);//keep only 10 digit Number
    // phoneNumber = phoneNumber.substring(phoneNumber.length - 10, -11);//keep only 10 digit Number
    phoneNumber = last
      ? phoneNumber.substring(phoneNumber.length - 10, phoneNumber.length)
      : phoneNumber.substring(0, 10);

    const cleaned = ('' + phoneNumber).replace(/\D/g, '');

    function numDigits(x: number) {
      return (Math.log(x) * Math.LOG10E + 1) | 0;
    }

    // only keep number and +
    const p1 = cleaned.match(/\d+/g);
    if (p1 == null) {
      return cleaned;
    }
    const p2 = phoneNumber.match(/\d+/g).map(Number);
    const len = numDigits(p2);
    // document.write(len + " " );
    switch (len) {
      case 1:
      case 2:
        return '(' + phoneNumber;
      case 3:
        return '(' + phoneNumber + ')';
      case 4:
      case 5:
      case 6:
        var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        var f2 = phoneNumber.toString().substring(len, 3);
        return f1 + ' ' + f2;
      default:
        f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        f2 = phoneNumber.toString().substring(3, 6);
        var f3 = phoneNumber.toString().substring(6, 10);

        return f1 + ' ' + f2 + '-' + f3;
    }
  }
  getOnlyDigits(phoneNumber) {
    const numberString = phoneNumber.toString();
    const numberInDigits = numberString.replace(/[^\d]/g,'');
    const numberVal = parseInt(numberInDigits, 10);
    return numberVal.toString();
  }

  checkValidationPhoneFOrmat(v, k) {
    let flag = true;

    console.log("A")
    switch (k) {
      case '+1':
        console.log("B")
        if (!v) {
          console.log("C")
          flag = true;
        } else {
          console.log("D")
          let ph = this.getOnlyDigits(v);
          console.log("D2", ph)
          flag = /^\s*[0-9]{1,10}\s*$/.test(ph);
        }

        break;

      case '+504':
        console.log("E")
        if (!v) {
          console.log("F")
          flag = true;
        } else {
          console.log("G")
          let ph = this.getOnlyDigits(v);

          flag = /^\s*[0-9]{1,8}\s*$/.test(ph);
          console.log("G2", ph, flag)
        }

        break;

      default:
        console.log("H")
        break;
    }

    console.log("I", flag)
    return flag;
  }

}
