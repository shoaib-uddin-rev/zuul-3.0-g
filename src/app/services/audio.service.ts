import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  audio: any;
  recording: any;
  received: boolean = false;

  constructor(
    public network: NetworkService,
    public platform: Platform) {
    // console.log('Hello AudioProvider Provider');
  }


  preload(){

    this.network.getSoundLocation().then( (data) => {

      if(data['bool'] == true){
        this.received = true;
        this.audio = new Audio();
        this.audio.src = data["result"]['sound'];
        this.audio.load();
      }
    }, err => {})
  }

  play(key: string): void {

    if(this.received == true){
      this.audio.play();
    }

  }

  playRecording(url) {
    if (url) {
      this.recording = new Audio();
      this.recording.src = url;
      this.recording.load();
      this.recording.play();
    }
  }

  pauseRecording() {
    this.recording.pause();
  }
}
