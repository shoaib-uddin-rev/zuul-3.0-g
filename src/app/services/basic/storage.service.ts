import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private nativeStorage: NativeStorage, private platform: Platform) { }

  set(key, data): Promise<boolean> {

    return new Promise(resolve => {
      if (this.platform.is("cordova")) {
        this.nativeStorage.setItem(key, data)
          .then(
            () => resolve(true),
            error => resolve(false)
          );
      } else {
        localStorage.setItem(key, data);
        resolve(key);
      }

    });

  }

  get(key): Promise<any> {

    return new Promise(resolve => {
      if (this.platform.is("cordova")) {
        this.nativeStorage.getItem(key)
          .then(
            () => resolve(true),
            error => resolve(false)
          );
      } else {
        const v = localStorage.getItem(key);
        resolve(v);
      }
    });

  }

}
