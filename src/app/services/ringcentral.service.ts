import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';

@Injectable({
    providedIn: 'root'
})

export class RingCentral {

    server_url = 'https://platform.devtest.ringcentral.com';
    // client_secret = 'CNCPe6-sRQeRMsFoErgHnQ';

    constructor(
        public http: HttpClient,
        public network: NetworkService
    ) { }

    // getToken(data) {
    //     return new Promise(async resolve => {
    //         // const phone = data.phone + '*' + data.ext;
    //         // const password = data.password;
    //         // const end_point = '/restapi/oauth/token';
    //         // const body = {
    //         //     grant_type: 'password',
    //         //     username: phone,
    //         //     password: password
    //         // };

    //         // const headers = {
    //         //     Authorization: 'Basic ' + this.client_secret,
    //         //     Accept: 'application/json',
    //         //     'Content-Type': 'application/x-www-form-urlencoded',
    //         // };

    //         // const res = this.http.post(this.server_url + '/' + end_point, body, { headers });
    //         // resolve(res);
    //     })
    // }

    getCallLogs() {
        return new Promise(async resolve => {
            const data = await this.network.getRingCentralCallLogs();
            // console.log(data);
            resolve(data);
        })
    }

    getMessageList() {
        return new Promise(async resolve => {
            const data = await this.network.getRingCentralMsgList();
            // console.log(data);
            resolve(data);
        })
    }

    deleteMessage(token) {
        // https://platform.devtest.ringcentral.com/restapi/v1.0/account/~/extension/~/message-store/10519606005
        const headers = {
            Authorization: 'Bearer ' + token,
            Accept: '*/*',
        };

        // const res = this.http.post(this.server_url + '/' + end_point, body, { headers });
    }
}