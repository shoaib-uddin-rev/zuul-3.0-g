/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import { AudioService } from './audio.service';
import { EventsService } from './events.service';
import { NetworkService } from './network.service';
import {
  AngularFireDatabase,
  AngularFireList,
} from '@angular/fire/compat/database';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  users: AngularFireList<any[]>;

  constructor(
    public events: EventsService,
    public network: NetworkService,
    private fcm: FCM,
    public audio: AudioService,
    public af: AngularFireDatabase
  ) {
    this.assignEvents();
  }

  assignEvents() {
    this.events.subscribe(
      'user:settokentoserver',
      this.setTokenToServer.bind(this)
    );
  }

  async setTokenToServer() {
    // const fcm_token = await this.getFCMToken();
    // console.log('fcm_token', fcm_token);
    // if (fcm_token) {
    //   this.network.saveFcmToken({ token: fcm_token }).then(
    //     (data) => {},
    //     (err) => {
    //       console.error(err);
    //     }
    //   );
    // }
  }

  async getFCMToken() {
    return new Promise((resolve) => {
      const token = localStorage.getItem('fcm_token');
      console.log(token);
      if (token) {
        resolve(token);
      }
      resolve(null);
    });
  }

  getData(key) {
    const reference = this.af.list<any>(key + '/');
    reference.snapshotChanges().subscribe((value) => {
      const item = value.map((changes) => {
        const data = changes.payload.val();
        const id = changes.payload.key;
        return { id, ...data };
      });

      console.log(item);
    });
  }

  addData(name) {
    const reference = this.af.list<any>('users/');
    const res = reference.push({
      name: name,
    });
  }

  removeData(key) {
    const reference = this.af.list<any>('users/');
    const res = reference.remove(key);
  }

  updateData(key, update) {
    const reference = this.af.list<any>('users/');
    const res = reference.update(key, { name: update });
  }
}
