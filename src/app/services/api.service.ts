import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config/main.config';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  url: string;

  constructor(public http: HttpClient) {
    // const u = localStorage.getItem('baseurl');
    this.url = Config.api;
  }

  getUrl() {
    const u = localStorage.getItem('baseurl');
    this.url = /*u ? u : */  Config.api;
    return this.url;
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    return this.http.get(this.getUrl() + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.getUrl() + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.getUrl() + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.getUrl() + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.getUrl() + '/' + endpoint, body, reqOpts);
  }
}
