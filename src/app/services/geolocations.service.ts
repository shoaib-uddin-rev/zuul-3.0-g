import { Injectable } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { NetworkService } from './network.service';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class GeolocationsService {

  appointed_latlongs = [];

  constructor(
    private network: NetworkService
  ) { }

  getCoordsForGeoAddress(address, _default = true) {

    var self = this;
    return new Promise(resolve => {
      var self = this;
      var geocoder = new google.maps.Geocoder;
      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {


            var loc = results[0].geometry.location
            var lat = loc.lat();
            var lng = loc.lng();
            resolve({ lat: lat, lng: lng })

          } else {
            resolve(null);
          }
        } else {
          console.log({ results, status })
          resolve(null)
        }
      });
    })

  }

  getCoordsViaHTML5Navigator() {

    return new Promise((resolve) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          resolve(pos)

        }, function () {
          resolve({ lat: 51.5074, lng: 0.1278 });
        });
      } else {
        // Browser doesn't support Geolocation
        resolve({ lat: 51.5074, lng: 0.1278 });
      }
    })


  }



  async geoFencing() {
    return new Promise(async (resolve) => {
      let isWithin = false;
      const coords = await this.getCurrentLocationCoordinates();
      let data: any; // await this.network.getAppointedLatLongsForGuard();

      if (data.is_guard_geo_fence != "inactive") {
        this.appointed_latlongs = data.list;
        // console.log(this.appointed_latlongs);

        for (let i = 0; i < this.appointed_latlongs.length; i++) {
          const element = this.appointed_latlongs[i];
          let dist = await this.getDistBetweenLatlng(coords["lat"], coords["lng"], element.lat, element.long);
          // console.log(dist);
          if (dist < 100) {
            isWithin = true;
          }
        }

        console.log(isWithin);

      } else {
        isWithin = true;
      }


      resolve(isWithin);
      // if (dist > 100) {
      //   resolve(false);
      // } else {
      //   resolve(true);
      // }
    })
  }

  getCurrentLocationCoordinates() {
    return new Promise(async resolve => {
      Geolocation.getCurrentPosition().then((res) => {
        var lt = res.coords.latitude;
        var lg = res.coords.longitude;

        resolve({ lat: lt, lng: lg });
      });

    })
  }

  getDistBetweenLatlng(lat1, lon1, lat2, lon2, unit = 'F') {
    return new Promise(async resolve => {
      //:::    unit = the unit you desire for results                               :::
      //:::           where: 'M' is statute miles (default)                         :::
      //:::                  'K' is kilometers                                      :::
      //:::                  'N' is nautical miles                                  :::
      //:::
      // console.log(lat1, lon1, lat2, lon2);
      if ((lat1 == lat2) && (lon1 == lon2)) {
        resolve(0);
      }
      else {
        let radlat1 = Math.PI * lat1 / 180;
        let radlat2 = Math.PI * lat2 / 180;
        let theta = lon1 - lon2;
        let radtheta = Math.PI * theta / 180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
          dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") { dist = dist * 1.609344 }
        if (unit == "N") { dist = dist * 0.8684 }
        if (unit == "F") { dist = dist * 5280 }
        resolve(dist);
      }

    })
  }
}
