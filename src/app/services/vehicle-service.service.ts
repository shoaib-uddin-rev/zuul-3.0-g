import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleServiceService {

  constructor(
    public utility: UtilityService,
    public network: NetworkService ) {

  }

  getVehiclesList(): Promise<any[]>{
    return new Promise( resolve => {
      this.network.getVehicleList().then( data => {

        let vehicles = data['vehicles'];
        if(vehicles){
          resolve(vehicles);
        }else{
          resolve([]);
        }


      }, err => resolve([]) )
    })
  }

  addVehicle(data){
    return new Promise( resolve => {
      this.network.addVehicle(data).then( data => {

        let vehicles = data;
        if(vehicles){
          resolve(vehicles);
        }else{
          resolve(null);
        }


      }, err => resolve(null) )
    })
  }

  editVehicle(id, data){
    return new Promise( resolve => {
      this.network.editVehicle(id, data).then( data => {

        let vehicles = data;
        if(vehicles){
          resolve(vehicles);
        }else{
          resolve(null);
        }


      }, err => resolve(null) )
    })
  }

  async deleteVehicle(id){

    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Delete Vehicle?', 'Passes Attached with this vehicle will not be effected' );
    if(!flag){ return };


    return new Promise( resolve => {
      this.network.deleteVehicle(id).then( data => {
        resolve({})
      }, err => resolve(null) )
    })
  }

  setDefaultVehicle(id){
    return new Promise( resolve => {
      this.network.setDefaultVehicle(id).then( data => {
        resolve({})
      }, err => resolve(null) )
    })
  }

}
