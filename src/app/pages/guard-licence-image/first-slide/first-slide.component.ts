import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ControlContainer, FormGroup } from '@angular/forms';
import { BasePage } from '../../base-page/base-page';
const imgJson = require('../../../data/image.json');
import { Capacitor } from '@capacitor/core';
import { resolve } from 'dns';

@Component({
  selector: 'first-slide',
  templateUrl: './first-slide.component.html',
  styleUrls: ['./first-slide.component.scss'],
})
export class FirstSlideComponent extends BasePage implements OnInit {
  @Input() data: any;
  @Output() moveSlide: EventEmitter<any> = new EventEmitter<any>();
  @Output() kioskCallNextImage: EventEmitter<any> = new EventEmitter<any>();
  disableBtn: boolean = true;
  doorbird_cam: any;
  doorBirdInterval: any;
  doorBirdAvailable = true;
  camerasAvailable = false;
  doorbirdCamLoading: boolean;
  doorbirdImageUrl = '';
  enableDoorbird = true;
  reupload = false;
  public aForm: FormGroup;
  isSimulator = false;
  isCameraLoading = false;
  residentName: any;

  constructor(injecctor: Injector, private controlContainer: ControlContainer) {
    super(injecctor);
  }
  async ngOnInit(): Promise<void> {
    this.residentName = this.data['added_user']['fullName'];
    var toogleDoorbirdFlag = localStorage.getItem('enabledoorbird');
    if (toogleDoorbirdFlag == 'true') {
      this.enableDoorbird = true;
    } else {
      this.enableDoorbird = false;
    }
    this.aForm = <FormGroup>this.controlContainer.control;
    const camera_count = await this.network.getCameraCount();
    const doorbird = camera_count['doorbird'];
    this.doorBirdAvailable = doorbird > 0;
    const user = this.users.getActiveUser();
    if (this.doorBirdAvailable && user.doorbird_cam_id) {
      this.doorbirdCamLoading = true;
      const res = await this.network.setDoorbirdCameraToInitiate();
      var timer = 7;
      this.doorBirdInterval = setInterval(() => {
        this.getDoorBirdImage(res.mac_address);
        timer = timer - 1;
        if (timer == 0) {
          clearTimeout(this.doorBirdInterval);
          this.doorbirdCamLoading = false;
          this.doorBirdAvailable = false;
        }
      }, 1000);
    }
    this.disableBtn = false;
  }

  async ionViewWillEnter() {
    this.loopTotal = 0;
    console.log(this.loopTotal);
  }

  loopTotal = 0;
  loopFunctionCallKioskImage(scan_log_id, type) {

    return new Promise ( async resolve => {

      const res = await this.network.getUserLicenceImageFinalImage(
        scan_log_id,
        type
      );
      console.log(res, !res);

      if (!res) {
        this.loopTotal = this.loopTotal + 1;
        if (this.loopTotal != 7) {
          setTimeout(() => {
            this.loopFunctionCallKioskImage(scan_log_id, type);
          }, 1000);
        } else {
          resolve(true);
        }
        return;
      } else {
        this.loopTotal = 0;
        // this.isOpenNativeCamera = true;
        // update image here from link
        // this.data.added_user.license_image_url = res.fullImageUrl;
        this.settleGuardKioskImages(res);
        resolve(true);
      }

    })

  }

  async notVerified(flag) {
    if (!flag) {
      let isRemoteGuardEnable = this.users._user.enable_remote_guard;
      let remoteGuard = null;
      let check_toggle = localStorage.getItem('check_toggle');
      console.log(isRemoteGuardEnable, flag);
        if (isRemoteGuardEnable == 1) {
          if (check_toggle == 'true') {
            const obj = {
              scan_log_id: this.data.scan_log.id,
              image_type: 0, // driving licence ki type zero he
            };

            this.isCameraLoading = true;
            const res = await this.network.getUserLicenceImageViaKiosk(obj);
            await this.loopFunctionCallKioskImage(this.data.scan_log.id, 0);

            this.isCameraLoading = false;
            // setTimeout( async () => {
            //   const res = await this.network.getUserLicenceImageFinalImage(this.data.scan_log.id,1)
            //   console.log(res, !res);
            //   remoteGuard = res;
            //   this.settleGuardKioskImages(remoteGuard);
            // }, 5000);

            return;

          }

        }

      const getImage: any = !this.isSimulator
        ? await this.utility.snapImage('')
        : imgJson.image;
      let formdata = {
        id: this.data.user_id,
        license_image: getImage
          .replace('data:image/png;base64,', '')
          .replace('data:image/jpeg;base64,', '')
          .replace('data:image/jpg;base64,', ''),
        license_via_guard: true,
        license_type: 'driving_license',
        scan_log_id: this.data.scan_log.id,
      };
      if (getImage) {
        await this.network.updateProfileById(formdata['id'], formdata);
        this.reupload = true;
        // await this.utility.showAlert(
        //   'License Image uploaded, thank you for cooperation',
        //   'Upload Successful'
        // );
        this.data.added_user.license_image_url = getImage;
      }
    } else {
      let isRemoteGuardEnable = this.users._user.enable_remote_guard;
      let check_toggle = localStorage.getItem('check_toggle');
      if (isRemoteGuardEnable == 1) {
        // if (check_toggle == 'true') {
          this.kioskCallNextImage.emit({
            scan_log_id: this.data.scan_log.id,
            image_type: 1,
          });
        // }
      }
      this.moveSlide.emit(true);
    }
  }

  settleGuardKioskImages(ret) {
    console.log('dfsdgd', ret);

    if (ret.image_type == 'Driver License') {
      this.data.added_user.license_image_url = ret.fullImageUrl;
      this.reupload = true;
    }
  }

  async getDoorBirdImage(mac_address) {
    var obj = {
      mac_address: mac_address,
      scan_log_id: this.data.scan_log.id,
    };
    const api_res = await this.network.getDoorBirdCamInfo(obj);
    // this.timer--;
    // if (this.timer == 0) {
    //   clearInterval(this.doorBirdInterval);
    // }
    if (api_res['ImageURL']) {
      this.doorbird_cam = api_res;
      this.doorbirdCamLoading = false;
      this.doorBirdAvailable = true;
      // await this.uploadLicenseImage();
      // this.data.added_user.license_image_url = this.doorbird_cam?.ImageURL;
      this.doorbirdImageUrl = this.doorbird_cam?.ImageURL;
      clearInterval(this.doorBirdInterval);
    }
  }

  async uploadLicenseImage() {
    let flag = await this.utility.presentConfirm(
      'Yes',
      'No',
      'Are You Sure?',
      'Are you Sure you want to update license image?'
    );
    if (!flag) {
      return;
    }
    this.data.added_user.license_image_url = this.doorbird_cam?.ImageURL;
    let formdata = {
      id: this.data.user_id,
      license_image: this.doorbird_cam?.ImageURL,
      license_via_guard: true,
      license_type: 'driving_license',
      scan_log_id: this.data.scan_log.id,
    };
    await this.network.updateProfileById(formdata['id'], formdata);
    await this.utility.showAlert(
      'License Image uploaded, thank you for cooperation',
      'Upload Successful'
    );
  }

  hideYesButtonLogic() {
    //(data?.added_user.formattedPhone != '(111) 1111-111' && !reupload) ? true : false
    // let f = this.data;
    // console.log(
    //   'fumi',
    //   this.data?.added_user.formattedPhone,
    //   this.data?.added_user.formattedPhone == '(111) 111-1111'
    // );

    if (this.data?.added_user.formattedPhone == '(111) 111-1111') {
      return !this.reupload;
    }

    if (
      this.utility.getOnlyDigits(this.data?.added_user.formattedPhone) ==
      '11111111'
    ) {
      return !this.reupload;
    }

    return false;
  }

  getImageViaKiosk() {
    return imgJson.image;
  }
}
