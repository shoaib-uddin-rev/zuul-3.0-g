import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NetworkService } from 'src/app/services/network.service';
import { BasePage } from '../../base-page/base-page';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-open-door',
  templateUrl: './open-door.component.html',
  styleUrls: ['./open-door.component.scss'],
})
export class OpenDoorComponent implements OnInit {
  @Input('scan_log_id') scan_log_id: any;
  @Input('web_relay_id') web_relay_id: any;
  @Input('ignore_scan_log_id') ignore_scan_log_id = false;
  loading = false;
  @Output('output') output: EventEmitter<any> = new EventEmitter<any>();

  constructor(private network: NetworkService, public modals: ModalService, public users: UserService) {
  }

  ngOnInit() {}

  async openDoor() {

    // if (this.loading) {
    //   return;
    // }
    // const user = this.users._user;
    // const web_relay_id = user.selected_web_relay ? user.selected_web_relay : '';

    let isRemoteGuardEnable = this.users._user.enable_remote_guard;
    let remoteGuard = null;

    // console.log(isRemoteGuardEnable, flag);

    if(isRemoteGuardEnable == 1){

      const obj = {
        "scan_log_id": this.scan_log_id,
      }
      console.log('dsgfd',obj);


      this.loading = true;
      this.network.openGateViaKiosk(obj).then( v => {

        this.output.emit({
          timestamp: Date.now()
        });

        if(this.ignore_scan_log_id){
          this.closeModal()
        }

        this.loading = false;
      }, err => {
        // this.closeModal()
        this.loading = false;
      });





      // setTimeout( async () => {
      //   const res = await this.network.getUserLicenceImageFinalImage(this.data.scan_log.id,1)
      //   console.log(res, !res);
      //   remoteGuard = res;
      //   this.settleGuardKioskImages(remoteGuard);
      // }, 5000);



      return;

    }
    const obj = {
      web_relay_id: this.web_relay_id,
      scan_log_id: this.scan_log_id,
      ignore_scan_log_id: this.ignore_scan_log_id
    };
    console.log(obj, 'ASMNHFCIUKJSDHBF');

    this.loading = true;
    this.network.openWebRelay(obj).then((res) => {
      this.loading = false;
      // this.utility.presentSuccessToast('gate opened');
      // if (res) {
        //this.modals.dismiss();
      // }
      this.output.emit({
        timestamp: Date.now()
      });

      if(this.ignore_scan_log_id){
        this.closeModal()
      }

    }, (err) => {
      this.output.emit({
        timestamp: Date.now()
      })

      if(this.ignore_scan_log_id){
        this.closeModal()
      }
    }).catch( (err) => {
      this.output.emit({
        timestamp: Date.now()
      })

      if(this.ignore_scan_log_id){
        this.closeModal()
      }
    });
  }

  closeModal(){
    this.modals.dismiss();
  }
}
