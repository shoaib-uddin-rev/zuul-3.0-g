import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GuardLicenceImageComponent } from './guard-licence-image.component';

describe('GuardLicenceImageComponent', () => {
  let component: GuardLicenceImageComponent;
  let fixture: ComponentFixture<GuardLicenceImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardLicenceImageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GuardLicenceImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
