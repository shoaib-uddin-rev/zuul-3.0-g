import {
  Component,
  OnInit,
  AfterViewInit,
  Injector,
  Input,
  ViewChild,
  Output,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IonSlides } from '@ionic/angular';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { BasePage } from '../base-page/base-page';
import { PrinterService } from 'src/app/services/printer.service';
import { StreamScreenPage } from '../stream-screen/stream-screen.page';
import { CreateVehiclePageComponent } from '../create-vehicle-page/create-vehicle-page.component';
import { OpenDoorComponent } from './open-door/open-door.component';
import { Printer } from '@awesome-cordova-plugins/printer/ngx';

@Component({
  selector: 'app-guard-licence-image',
  templateUrl: './guard-licence-image.component.html',
  styleUrls: ['./guard-licence-image.component.scss'],
})
export class GuardLicenceImageComponent
  extends BasePage
  implements OnInit, AfterViewInit
{
  @Input() data;
  @Output() residentName;
  @ViewChild('slides', { static: true }) slides: IonSlides;
  // @ViewChild('ocr_input', { static: true }) ocr_input: IonInput;
  aForm: FormGroup;
  submitAttempt = false;
  liImageBase64 = '';
  profile: any = {};
  user: any = {};
  tempaccesstoken: any;
  date_of_birth_mdy = '';
  revisit = false;
  islicence_must = false;
  slideNumber = 0;
  heading1 = 'ID Verification';
  heading2 = 'License Plate Verification';
  isCameraLoading = false;
  interval;
  api_res: any;
  // showLicenseImageReplace = false;
  rover_mode = false;
  showOcr = true;
  isOpenNativeCamera;
  isFixedCam = false;
  camera: any = {
    capture_progress: true,
    ImageURL: '',
    ocr: '',
  };

  isNoCameraAvailable = false;
  isStandard = false;
  camerasAvailable = false;
  timer = 7;
  dummy_data = {
    capture_progress: false,
    camera_name: 'Camera 6 ',
    ocr: 'R0lGO',
    mac_address: '134:1364:464',
    ImageID: 'car_09112021_1',
    ImageURL:
      'https://firebasestorage.googleapis.com/v0/b/zuul-master.appspot.com/o/images%2Fcar_18112021_1.jpg?alt=media&token=ea291061-ce2b-4c59-825c-86f77eeb6aa9',
  };

  printer = null;
  self;

  constructor(injector: Injector, public geolocation: GeolocationsService) {
    super(injector);
    this.setupForm();

    this.printer = new PrinterService(new Printer());

    this.goToNext(0);

    const res = this.nav.getQueryParams().res;
    this.data = JSON.parse(res);

    if (this.data.added_user.vehicle) {
      this.camera.ocr = this.data.added_user.vehicle.license_plate;
      delete this.data.added_user.vehicle.user_id;
    }

    this.data['pass_user_id'] = this.data.id;
    this.data = { ...this.data, ...this.data.added_user.vehicle };

    this.initialKioskHit();
    // this.callKioskImagesVIaCamera().then( v => {

    // if(v){
    this.getCameras();
    // }
    // });

    // this.events.subscribe('get-licence-plate-via-kiosk', async (x) => {
    //   console.log('abdeer',x);
    //   const obj = {
    //     "scan_log_id": this.data.scan_log.id,
    //     "image_type": 1 // driving licence ki type zero he
    //   }
    //   const res = await this.network.getUserLicenceImageViaKiosk(x);
    //   this.loopTotal = 0
    //   this.loopFunctionCallKioskImage( x["scan_log_id"], x["image_type"])
    // })
  }

  ngOnInit(): void {
    this.self = this;
  }

  initialKioskHit() {
    this.callKioskImagesVIaCamera();
  }

  getCameras() {
    let isRemoteGuardEnable = this.users._user.enable_remote_guard;
    let check_toggle = localStorage.getItem('check_toggle');

    if (isRemoteGuardEnable == 1) {
      if (check_toggle == 'true') {
        return;
      }
    }
    console.log('getcamera');

    this.network.getCameraCount().then((v) => {
      this.data['added_user']['fullName'] = this.residentName;

      const camera_count = v;
      console.log(' asfdf', camera_count);
      var lpr = camera_count['lpr'];
      var non_lpr = camera_count['non-lpr'];

      this.setFlags();

      this.camerasAvailable = lpr + non_lpr > 0;
    });
  }

  ngAfterViewInit(): void {
    this.slides.update();
    this.slides.lockSwipes(true);
  }

  async ionViewWillEnter() {
    this.loopTotal = 0;
    console.log(this.loopTotal);
    this.camera.ocr = null;
  }

  async loopFunctionCallKioskImage(scan_log_id, type) {
    return new Promise(async (resolve) => {
      const res = await this.network.getUserLicenceImageFinalImage(
        scan_log_id,
        type
      );
      console.log(res, !res);

      if (!res) {
        this.loopTotal = this.loopTotal + 1;
        if (this.loopTotal < 7) {
          setTimeout(() => {
            this.loopFunctionCallKioskImage(scan_log_id, type);
          }, 500);

        } else {
          this.isCameraLoading = false;
        }


        return;
      } else {
        this.loopTotal = 0;
        console.log("sfdfd");

        // update image here from link
        // this.data.added_user.license_image_url = res.fullImageUrl;
        await this.settleGuardKioskImages(res);
        resolve(true);
      }
    });
  }

  settleGuardKioskImages(ret) {
    return new Promise((resolve) => {
      if (ret.image_type == 'License Plate') {
        this.isFixedCam = true;
        this.camera.ImageURL = ret.fullImageUrl;
        console.log('adfsgds',this.camera.ImageURL);
        if (ret.plateNum) {
          this.camera.ocr = ret.plateNum;
        }
        this.isCameraLoading = false;
        // return;
      }

      resolve(true);
    });
  }

  callKioskImagesVIaCamera(type = 0) {
    return new Promise(async (resolve) => {
      let isRemoteGuardEnable = this.users._user.enable_remote_guard;
      let remoteGuard = null;
      // if (check_toggle == 'true') {
        if (isRemoteGuardEnable == 1) {
          this.isCameraLoading = true;
          const obj = {
            scan_log_id: this.data.scan_log.id,
            image_type: type, // driving licence ki type zero he
          };

          this.network.getUserLicenceImageViaKiosk(obj).then(async (v) => {
            console.log('check187');
            this.loopTotal = type;
            setTimeout( async () => {
              await this.loopFunctionCallKioskImage(this.data.scan_log.id, type);
            }, 2000);

          });;




          console.log('check189');

          // setTimeout( async () => {
          //   const res = await this.network.getUserLicenceImageFinalImage(this.data.scan_log.id,1)
          //   console.log(res, !res);
          //   remoteGuard = res;
          //   this.settleGuardKioskImages(remoteGuard);
          // }, 5000);

          resolve(true);
          return;
        }
      // }
      console.log('bahir hai');

      resolve(true);
    });
  }

  async setFlags() {
    let user = this.users.getCurrentUser();
    this.rover_mode = user.rover_mode == 0 ? false : true;
    if (!this.rover_mode) {
      this.openCameras();
    } else if (this.rover_mode) {
      const rover_type = user.rover_type;
      console.log({ rover_type });
      if (rover_type == 'regular') {
        this.showOcr = false;
        this.isOpenNativeCamera = false;
      } else {
        this.isOpenNativeCamera = true;
      }
    } else {
      this.showOcr = false;
      this.isOpenNativeCamera = false;
    }
  }

  ocrChange($event) {
    // console.log($event);
    // let v = $event.target.value;
    // if (v) {
    //   $event.target.value = v.toUpperCase();
    // }
  }

  updateLicensePlate() {
    console.log(this.user);
    return new Promise<void>(async (resolve) => {
      const obj = {
        license_plate_image: this.camera?.ImageURL,
      };
      await this.network.updateLicenseImage(this.data.scan_log.id, obj);
      resolve();
    });
  }

  updateOCR() {
    return new Promise(async (resolve) => {
      const obj = {
        user_id: this.data.user_id,
        pass_user_id: this.data.pass_user_id,
        license_plate: this.camera.ocr,
        scan_log_id: this.data.scan_log.id,
      };

      console.log(obj);

      this.network
        .addVehicleByGuard(obj)
        .then((res) => {
          console.log(res);
          resolve(res);
        })
        .catch((err) => {
          console.log(err);
          const error = err.error;
          this.utility.presentFailureToast(error.message);
          console.log(
            'error.is_licece_banned',
            error.is_licece_banned,
            error.is_licece_banned == 1
          );
          if (error.is_licece_banned == 1) {
            this.nav.pop();
          }

          resolve(null);
        });
    });
  }

  async finishSeq() {
    this.isCameraLoading = true;
    console.log('kadhfis', this.showOcr);

    if (this.showOcr) {
      const res = await this.updateOCR();
      console.log('resolve-null', res);
      if (!res) {
        this.isCameraLoading = false;
        console.log('resolve-null9', res);
        // this.nav.pop();
        // this.modals.dismiss({
        //   timestamp: Date.now()
        // })
        console.log('sDFsdfsdg');

        return;
        //return this.utility.presentFailureToast('Failed, Please type in license plate');
      }

      // if(res['is_licece_banned'] == '1'){
      //   return this.utility.presentFailureToast(res['message']);
      // }
    }

    await this.updateLicensePlate();
    this.isCameraLoading = false;

    const air_print = this.users._user.air_print;
    if (air_print == true || air_print == 'true') {
      if (this.data.scan_log.id) {
        const template = await this.network.getPrinterTemplate(
          this.data.scan_log.id
        );
        if (template.template) {
          await this.printer.print(template.template);
          this.printer = null;
        }
      }
    }

    this.notVerified('licence_verified', true);
    // this.dismissScreen();
  }
  dismissScreen() {
    this.nav.pop();
    //  this.modals.dismiss({ timestamp: Date.now() });
  }
  async setupForm() {
    const re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      license_format: ['', Validators.compose([Validators.required])],
      license_image: ['', Validators.compose([Validators.required])],
      image_verified: [''],
      license_verified: [''],
    });

    this.user = JSON.parse(localStorage.getItem('user'));
  }

  bolan($event) {
    this.closeModal({ data: 'A' });
  }

  async kioskCallNextImage($event) {
    const obj = {
      scan_log_id: this.data.scan_log.id,
      image_type: 1, // driving licence ki type zero he
    };

    this.isCameraLoading = true;

    const res = await this.network.getUserLicenceImageViaKiosk($event);
    this.loopTotal = 0;
    console.log('chekids');

    await this.loopFunctionCallKioskImage(
      $event['scan_log_id'],
      $event['image_type']
    );

    setTimeout(() => {
    }, 2000);
  }

  async notVerified(type, flag) {
    switch (type) {
      case 'licence_verified':
        this.aForm.controls.license_verified.setValue(flag);
        console.log('afddgs', flag);

        if (!flag) {
          let isRemoteGuardEnable = this.users._user.enable_remote_guard;
          let check_toggle = localStorage.getItem('check_toggle');
          if (isRemoteGuardEnable == 1) {
            console.log('callKiosk');

            this.callKioskImagesVIaCamera(1);
            return;
          }
          console.log('not callKO+IOS');

          const res = await this.modals.present(CreateVehiclePageComponent, {
            item: this.data,
            scan_log_id: this.data.scan_log.id,
          });
          const vehicledata = res.data;

          if (vehicledata.data != 'A') {
            const vehicle = vehicledata;
            delete vehicle.user_id;
            this.data.license_plate = vehicle.license_plate;
            this.data.make = vehicle.make;
            this.data.model = vehicle.model;
            this.data.year = vehicle.year;
            this.data.color = vehicle.color;

            const obj = {
              pass_user_id: this.data.pass_user_id,
              vehicle_id: vehicle.id,
              scan_log_id: this.data.scan_log.id,
            };

            this.network.setPassVehicle(obj).then(async (v) => {
              this.bu6789687();
              // this.licencePlateThankyou();
            });
            this.isCameraLoading = false;
          }
        } else {
          // await this.utility.showAlert(
          //   'Thank you for cooperation',
          //   'Upload Successful'
          // );
          // await this.utility.presentSuccessToast(
          //   'Upload Successful, Thank you'
          // );
          // add a check here if user has set a web-relay id
          // this.closeModal({ data: 'A' });
          this.bu6789687()
        }
        break;

      case 'image_verified':
        this.aForm.controls.image_verified.setValue(flag);
        this.moveSlide();
        break;
    }
  }

  async bu6789687() {
    const user = this.users._user;
    const web_relay_id = user.selected_web_relay
      ? user.selected_web_relay
      : null;

    // call the api and get its response
    // if response is 1 then call the below if condition
    const res = await this.network.getWebRelayStatus();
    console.log(res.web_relay_status);
    if (res.web_relay_status == 1) {
      console.log('dfa', web_relay_id);

      if (web_relay_id || !web_relay_id) {
        this.isCameraLoading = false;
        this.goToNext(2);

        // this.nav.pop().then( (v) => {
        // this.modals.present(OpenDoorComponent, {
        //   scan_log_id: this.data.scan_log.id,
        //   web_relay_id: web_relay_id,
        //   ignore_scan_log_id: true
        // }).then.this.nav.pop().then( (v) => {;

        // this.modals.present(OpenDoorComponent, {
        //   scan_log_id: this.data.scan_log.id,
        // });
        // });
      }
    } else {
      this.closeModal({ data: 'A' });
      console.log('asfsdgfds', this.data);
    }
  }
  async licencePlateThankyou() {
    // const confirm = await this.utility.showAlert(
    //   'License plate number uploaded, thank you for cooperation',
    //   'Upload Successful'
    // );
    this.closeModal({ data: 'A' });
  }

  cam_data_global = null;
  async openCameras() {
    console.log('opencameras');

    // let cam_id = this.users._user.default_camera;
    // const latlng = await this.geolocation.getCurrentLocationCoordinates();

    // const data = await this.network.getCameras(latlng);

    // let cam_data = data.find((x: { id: any }) => x.id == cam_id);

    // if (!cam_data) {
    //   if (data && data.length > 0) {
    //     cam_data = data[0];
    //   } else {
    //     this.isOpenNativeCamera = true;
    //     return (this.isNoCameraAvailable = true);
    //   }
    // }

    this.isFixedCam = true;
    this.isOpenNativeCamera = false;
    let isRemoteGuardEnable = this.users._user.enable_remote_guard;
    let check_toggle = localStorage.getItem('check_toggle')
      if (isRemoteGuardEnable == 1) {
        if (check_toggle == 'true'){
        console.log('calledornot');
        let obj = {
          scan_log_id: this.data.scan_log.id,
          image_type: 1,
        }
        console.log('sdfghjkl;');

        this.kioskCallNextImage(obj)

      }
      // const cam_data = await this.network.initializeCamera({});

      // this.isCameraLoading = true;
      // var x = 0;
      // this.cam_data_global = cam_data;

      // this.loopFunctionCall();
      console.log('safdsgs');

    } else {
      console.log('ashbfyhvb');

      const cam_data = await this.network.initializeCamera({});
      console.log('sdafsdg', cam_data);

      this.isCameraLoading = true;
      var x = 0;
      this.cam_data_global = cam_data;
      this.loopFunctionCall();
    }
    // this.interval = setInterval(async () => {
    //   const res = await this.getCameraImage(cam_data);

    //   if(res){

    //   }else{

    //   }
    // this.getCameraImage(cam_data);
    // if (++x === 7) {
    //   if (!this.api_res) {
    //     this.rover_mode = true;
    //     const rover_type = this.user.rover_type;
    //     console.log({ rover_type });
    //     if (rover_type == 'regular') {
    //       this.showOcr = false;
    //       this.isOpenNativeCamera = false;
    //     } else {
    //       this.isOpenNativeCamera = true;
    //     }
    //   }
    //   clearInterval(this.interval);
    // }
    // }, 1000);
  }

  loopTotal = 0;
  async loopFunctionCall() {
    this.isCameraLoading = true;

    const res = await this.getCameraImage(this.cam_data_global);
    console.log('result', res);
    if (!res) {
      this.loopTotal = this.loopTotal + 1;
      if (this.loopTotal != 8) {
        setTimeout(() => {
          this.loopFunctionCall();
        }, 1000);
      } else {
        this.rover_mode = true;
        const rover_type = this.user.rover_type;
        console.log({ rover_type });
        if (rover_type == 'regular') {
          this.showOcr = false;
          this.isOpenNativeCamera = false;
        }
        this.isCameraLoading = false;
      }
      return;
    } else {
      // this.isOpenNativeCamera = true;
    }

    this.isCameraLoading = false;
  }

  openNativeCamera() {
    this.utility.snapImage('profile').then((image) => {
      this.camera.ImageURL = image as string;
    });
  }

  getCameraImage(camera) {
    return new Promise(async (resolve) => {
      // if (!this.camera.capture_progress) {
      // console.log('asdfdgrd', camera.id);
      let cid = camera.id;
      console.log(cid, 'crime investigation department');

      this.api_res = await this.network.getCameraInfo(cid);
      console.log('Api response: ', this.api_res);

      if (!this.api_res) {
        resolve(false);
        return;
      }

      if (!this.api_res['ImageURL']) {
        resolve(false);
        return;
      }

      this.camera = this.api_res;
      if (
        this.api_res.plateNum != 0 &&
        this.api_res.plateNum != null &&
        this.api_res.plateNum != ''
      ) {
        this.camera.ocr = this.api_res.plateNum;
      }
      if (this.data?.license_plate) {
        this.camera.ocr = this.data.license_plate;
      }

      resolve(true);
      return;

      // clearInterval(this.interval);

      // if (this.api_res) {
      //   if (this.api_res['ImageURL']) {
      //   }
      // }
      // }
    });
  }

  // getCameraInfo(id) {
  //   return this.dummy_data;
  // }

  clearCameraInterval() {
    clearInterval(this.interval);
  }

  moveSlide() {
    this.goToNext(1);
    let isRemoteGuardEnable = this.users._user.enable_remote_guard;
      if (isRemoteGuardEnable == 1) {
        return;
      }
    if (this.isNoCameraAvailable || this.rover_mode) {
      if (this.isOpenNativeCamera) {
        this.openNativeCamera();
      }
    }
    // this.ocr_input.setFocus();
  }

  goToNext(num) {
    this.slideNumber = num;
    console.log('mm', this.slides);
    if (this.slides) {
      this.slides.lockSwipes(false);
      this.slides.slideTo(num, 500);
      this.slides.lockSwipes(true);
    }
  }

  closeModal(res) {
    this.events.publish('scanlog:update');
    // this.modals.dismiss(res);
    this.nav.pop();
  }

  // async openStream(type = '') {
  //   if (type == 'doorbird') {
  //     return;
  //   }

  //   console.log(this.user);
  //   const { data } = await this.modals.present(StreamScreenPage, {
  //     camera_id: this.user.doorbird_cam_id,
  //     guard_id: this.user.id,
  //     scan_log: this.data,
  //     type: type,
  //   });
  // }

  hideforOnesPhone() {}
}
