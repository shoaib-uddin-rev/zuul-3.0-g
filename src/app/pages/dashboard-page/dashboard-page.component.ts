import { Component, Injector, OnInit, AfterViewInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { UpdatePasswordPageComponent } from '../update-password-page/update-password-page.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { RegisterPage } from '../register/register.page';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { SettingsPage } from '../settings/settings.page';
import { OnboardingPage } from '../onboarding/onboarding.page';
import { OpenDoorComponent } from '../guard-licence-image/open-door/open-door.component';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent extends BasePage implements OnInit {
  avatar = this.users.avatar;
  user: any;
  plist: any[] = [];
  page = 1;
  isWithinLocation: any = true;
  isLoading: boolean = false;
  isResidentsLoading: boolean;
  qrcodeLoading = false;

  constructor(
    injector: Injector,
    private barcodeScanner: BarcodeScanner,
    private geolocation: GeolocationsService,
    public menuCtrl: MenuController
  ) {
    super(injector);

    menuCtrl.enable(false);

    // this.firebase.addData("Hello new user");
    // this.firebase.removeData('-Mk7pV0wyKKEhuOI2cQY');
    // this.firebase.updateData('-Mk7qAuTPKMxVoIDN23l', "Hello new USer twice")
    // this.firebase.getData('users');

    this.initializeView();
    this.events.subscribe('scanlog:update', this.getScanHistory.bind(this));
    this.events.subscribe(
      'dashboard:initialize',
      this.initializeView.bind(this)
    );
    this.events.subscribe(
      'dashboard:updateprofile',
      this.openRegistrationPage.bind(this)
    );

    // initialize socket io connection
    // this.sockets.initialize()
  }

  async ngOnInit() {
    this.checkOnboarding();
  }

  async ionViewWillEnter() {
    await this.dumpResidentsIntoSqlite();
    this.doRefresh();
  }

  async checkOnboarding() {
    let user = await this.sqlite.getActiveUser();
    console.log(user);
    // const isOnboarded =  user.is_onboarded == '1' ? true : false;

    if (user['is_onboarded'] == '0') {
      this.presentOnboarding();
    }
  }

  async openRegistrationPage() {
    // this.menuCtrl.close();
    this.user = await this.sqlite.getActiveUser(); //JSON.parse(localStorage.getItem('user'));
    this.modals.present(RegisterPage, { new: false, user: this.user });
    // this.modals.present(RegisterPageComponent, { new: false, user: this.user });
  }

  async initializeView() {
    const self = this;
    this.user = await this.sqlite.getActiveUser();
    this.isWithinLocation = true; //await this.geolocation.geoFencing();

    this.getScanHistory().then(() => {
      if (this.user.is_reset_password == 1) {
        this.modals.present(UpdatePasswordPageComponent, { user: this.user });
      }
    });

    this.events.publish('user:settokentoserver');
  }

  async dumpResidentsIntoSqlite() {
    let params = {
      page: -1,
      search: '',
    };

    const user_id = await this.sqlite.getActiveUserId();
    console.log({ user_id }, 'on set');

    // await this.sqlite.deleteAllResidents(this.users.getActiveUser().id);
    let residentCounts = await this.sqlite.getCurrentUserResidentsCount(
      user_id
    );

    console.log({ residentCounts });

    var d = Date.now();

    var sync_time = localStorage.getItem('gesident_sync');
    var diff = -1;
    if (sync_time != null) {
      diff = d - parseInt(sync_time) > 36000 ? -1 : 0;
    } else {
      diff = -1;
    }

    // if (residentCounts == 0 && diff == -1) {
    if (residentCounts == 0 && diff == -1) {
      localStorage.setItem('resident_sync', `${d}`);
      let user_id = await this.sqlite.getActiveUserId();
      this.isResidentsLoading = true;

      await this.network.getGuardResidents(params).then(async (result) => {
        await this.sqlite.setResidentsInDatabase(
          result['list'],
          user_id
          // this.users.getActiveUser()
        );
        this.isResidentsLoading = false;
      });
    }
  }

  async presentOnboarding() {
    await this.modals.present(OnboardingPage);
    this.users.getUser();
  }

  doRefresh($event = null) {
    return new Promise(async (resolve) => {
      this.page = 1;
      // this.plist = [];
      await this.getScanHistory();
      if ($event) {
        $event.target.complete();
      }
      resolve(true);
    });
  }

  getScanHistory(data = {}, loader = true, search = false) {
    return new Promise((resolve) => {
      this.isLoading = true;

      const data = {
        expired: 0,
        page: this.page,
      };

      this.network.getScanHistory(data).then((res: any) => {
        if (this.page == 1) {
          this.plist = res.list;
        } else {
          this.plist = [...this.plist, ...res.list];
        }

        if (res.list.length > 0) {
          this.page = this.page + 1;
        }
        this.isLoading = false;
        resolve(true);
      });
    });
  }

  logout() {
    this.events.publish('user:logout');
  }

  loadMore($event) {
    this.getScanHistory().then((v) => {
      $event.target.complete();
    });
  }

  loopTotal = 0;
  loopFunctionCallKioskQrcode(socket_id) {
    return new Promise(async (resolve) => {
      this.network
        .receiveScanQrByKiosk(socket_id)
        .then(
          (res) => {
            console.log('responeOfRecieveQRSCANBYkiios', res);

            if (res?.qrcode) {
              // this.isOpenNativeCamera = true;
              // update image here from link
              // this.data.added_user.license_image_url = res.fullImageUrl;
              // this.loopFunctionCallKioskQrcode(socket_id);
              console.log(res.qrcode);
              this.loopTotal = 0;
              this.qrcodeLoading = false;
              this.callToScanQr(res.qrcode);
              resolve(true);
            } else {
              this.loopTotal = this.loopTotal + 1;
              if (this.loopTotal != 12) {
                setTimeout(() => {
                  this.loopFunctionCallKioskQrcode(socket_id);
                }, 5000);
              } else {
                this.utility.presentFailureToast('QRCode Scan Timeout');
                this.qrcodeLoading = false;
                resolve(false);
              }
              return;
            }
          },
          (err) => {
            console.error(err);
            resolve(false);
          }
        )
        .catch((err) => {
          console.error(err);
          resolve(false);
        });
    });
  }

  async scanCode(istext: boolean = false, qrcode: string = null) {
    let isRemoteGuardEnable = this.users._user.enable_remote_guard;
    let remoteGuard = null;
    let check_toggle = localStorage.getItem('check_toggle');
    console.log('afhgbdsf', check_toggle);
    if (check_toggle == 'true') {
      if (isRemoteGuardEnable == 1) {
        console.log('enale remote guard');
        // const res = await this.modals.present(KioskScanViewComponent)
        // const obj = {
        //   // scan_log_id: this.data.scan_log.id,
        //   // image_type: 1, // driving licence ki type zero he
        // };
        this.loopTotal = 0;
        this.qrcodeLoading = true;
        const res = await this.network.getScanQrByKiosk();
        if (res?.socket_id) {
          await this.loopFunctionCallKioskQrcode(res['socket_id']);
        }
        this.qrcodeLoading = false;
        // setTimeout( async () => {
        //   const res = await this.network.getUserLicenceImageFinalImage(this.data.scan_log.id,1)
        //   console.log(res, !res);
        //   remoteGuard = res;
        //   this.settleGuardKioskImages(remoteGuard);
        // }, 5000);
        return;
      }
    }

    // const qrcode = istext
    //   ? await this.utility.presentInput('Ok', 'Cancel', 'Input QRCode')
    //   : await this.barcodeScanner.scan();
    if (!this.isWithinLocation) {
      return this.utility.showAlert(
        'You should be within 100ft of your assigned Location to Scan !',
        'Out Of Range'
      );
    }

    // let qrcode;

    if (!qrcode) {
      if (!istext) {
        const res = await this.barcodeScanner.scan();
        qrcode = res.text;
      } else {
        qrcode = await this.utility.presentInput(
          'Ok',
          'Cancel',
          'Input QRCode'
        );
      }
    }

    if (!qrcode) {
      return;
    }

    this.callToScanQr(qrcode);
  }

  callToScanQr(qrcode) {
    return new Promise(async (resolve) => {
      const data = {
        type: 'guard',
        id: this.user.id,
        code: qrcode,
      };

      this.network.validateQrCode(data).then(
        async (res) => {
          const alertmode = res.alert_mode;
          if (alertmode.flag == true) {
            await this.utility.showAlert(alertmode.message);
          }

          await this.doRefresh();

          const data = res;
          // send scanlog_id
          console.log('DFGSDHFHJG', data);

          this.gotoGuardLicenceImage(data);
          resolve(true);
        },
        async (err) => {
          console.log(err);
          resolve(false);
          await this.doRefresh();
        }
      );
    });
  }

  async gotoGuardLicenceImage(data) {
    // const _data = await this.modals.present(GuardLicenceImageComponent, {
    //   data,
    // });
    this.nav.push('GuardLicenceImage', { res: JSON.stringify(data) });
  }

  // Init a timeout variable to be used below
  gotiGuardRes() {
    this.nav.push('GuardResidentsPage');
  }

  async openPopover($event) {
    const res = await this.popover.present($event, { flag: 'MENU' });

    const data = res.data;
    if (data) {
      switch (data.param) {
        case 'MA':
          this.openRegistrationPage();
          break;
        case 'L':
          this.logout();
          break;
        case 'S':
          this.openSettings();
          break;
      }
    }
  }

  openSettings() {
    this.modals.present(SettingsPage);
  }

  async openGate() {
    let user = localStorage.getItem('user');
    console.log('fdfsf', user);

    if (!user) {
      return;
    }

    let yuser = JSON.parse(user);
    const web_relay_id = yuser['selected_web_relay'];
    const res = await this.network.getWebRelayStatus();
    console.log('safdsgd', yuser);
    console.log(res, yuser, web_relay_id);

    if (res.web_relay_status == 1) {
      // if (!web_relay_id ) {
      //   alert('Please Select Web Relay');
      //   return;
      // }
      this.modals.present(OpenDoorComponent, {
        scan_log_id: -1,
        web_relay_id: web_relay_id,
        ignore_scan_log_id: true,
      });
    } else {
      this.utility.presentFailureToast('This Feature is not available');
    }
  }
}
