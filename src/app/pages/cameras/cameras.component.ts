import { Component, Injector, OnInit } from '@angular/core';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.scss'],
})
export class CamerasComponent extends BasePage implements OnInit {
  type;
  cameras: any[];
  onlySelect = false;
  isLoading;

  constructor(injector: Injector, private geolocation: GeolocationsService) {
    super(injector);
  }

  async ngOnInit() {
    await this.getCameras();
  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  doRefresh() {
    this.getCameras();
  }

  async getCameras() {
    console.log(this.type);
    this.isLoading = true;
    const data = {} // await this.geolocation.getCurrentLocationCoordinates();
    if (this.type == 'doorbird') {
      data['type'] = 'doorbird';
    }

    this.cameras = await this.network.getCameras(data);
    this.isLoading = false;
    console.log(this.cameras);
  }

  async openCamera(camera) {
    this.closeModal({ camera });
  }
}
