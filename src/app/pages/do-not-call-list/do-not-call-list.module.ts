import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoNotCallListPageRoutingModule } from './do-not-call-list-routing.module';
import { AddDncComponent } from './add-dnc/add-dnc.component';

import { DoNotCallListPage } from './do-not-call-list.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoNotCallListPageRoutingModule,
    Ng2SearchPipeModule,
    ReactiveFormsModule,
    EmptyviewComponentModule,
  ],
  declarations: [DoNotCallListPage, AddDncComponent, CountryCodeComponent],
})
export class DoNotCallListPageModule {}
