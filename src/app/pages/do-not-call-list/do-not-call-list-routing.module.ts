import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoNotCallListPage } from './do-not-call-list.page';

const routes: Routes = [
  {
    path: '',
    component: DoNotCallListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoNotCallListPageRoutingModule {}
