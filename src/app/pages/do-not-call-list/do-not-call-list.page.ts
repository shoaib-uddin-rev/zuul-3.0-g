import { Component, Injector, Input, OnInit } from '@angular/core';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { BasePage } from '../base-page/base-page';
import { ThrowStmt } from '@angular/compiler';
import { AddDncComponent } from './add-dnc/add-dnc.component';
import { ResidentsService } from 'src/app/services/residents.service';
import { QuickPassPageComponent } from './../quick-pass-page/quick-pass-page.component';

@Component({
  selector: 'app-do-not-call-list',
  templateUrl: './do-not-call-list.page.html',
  styleUrls: ['./do-not-call-list.page.scss'],
})
export class DoNotCallListPage extends BasePage implements OnInit {
  plist = [];
  page = 1;
  isLoading = false;
  isSync = false;
  offset = 0;
  search_value;

  @Input() resident_id;
  @Input() display_name;
  user_id;

  constructor(public injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    this.user_id = await this.sqlite.getActiveUserId()
  }

  ionViewWillEnter() {
    this.loadData();
  }

  async searchUpdate($event) {
    const v = $event.target.value;
    console.log(v);
    this.offset = 0;
    const dnc: any = await this.sqlite.getDoNotCallList( this.user_id, v, this.offset);
    this.offset = dnc.offset;
    this.plist = dnc.list;
  }

  async loadData(resync = false) {
    this.isLoading = true;

    // const userId = await this.sqlite.getActiveUserId();
    // const count = await this.sqlite.getCurrentUserDoNotCallListCount(
    //   this.resident_id
    // );

    // console.log('count', count, count == 0);
    // if (count == 0) {
    const res = await this.network.getContactInDncByResidentId(
      this.resident_id
    );
    console.log('ui', res);

    this.plist = res['list'];


    this.isLoading = false;
    await this.dumpRecordsIntoSqlite(res['list']);
    // } else {
    //   this.getAllDNCList(this.search_value, this.offset);
    //   this.isLoading = false;
    // }
  }

  async getAllDNCList(search, offset) {

    return new Promise( async resolve => {

      const dnc: any = await this.sqlite.getDoNotCallList(
        this.resident_id,
        search,
        offset,
      );
      if (this.offset == 0) {
        this.plist = dnc.list;
      } else {
        this.plist = [...this.plist, ...dnc.list];
      }
      console.log({ dnc });

      this.offset = dnc.offset;

      resolve(true)

    })

  }

  async dumpRecordsIntoSqlite(list) {

    this.isLoading = true;
    // const dncRecods: any = await this.residents_service.fakeDncRecords();
    await this.sqlite.setDoNotCallListInDatabase(list, this.resident_id);
    await this.getAllDNCList(this.search_value, this.offset);
    this.isLoading = false;
  }

  async doRefresh($event) {
    // const userId = await this.sqlite.getActiveUserId();
    // await this.sqlite.deleteDoNotCallListByUserId(userId);
    this.loadData();
    $event.target.complete();
  }

  async loadMore($event) {
    if (!this.search_value) {
      if (this.offset == -1) {
        $event.target.complete();
      } else {
        await this.getAllDNCList(this.search_value, this.offset);
        $event.target.complete();
      }
    }
  }

  goBack() {
    // this.location.back();
    this.modals.dismiss({
      data: 'A',
    });
  }

  reset() {
    this.offset = 0;
    this.search_value = '';
    this.getAllDNCList(this.search_value, this.offset);
  }

  async sync() {
    // const res = await this.modals.present(AddDncComponent);
    this.isLoading = true;
    this.isSync = true;
    const res = await this.network.getContactInDncByResidentId(
      this.resident_id
    );
    console.log(res);
    this.plist = res['list'];
    this.isSync = false;
    this.isLoading = false;
    this.offset = 0;
    this.search_value = '';
    await this.dumpRecordsIntoSqlite(res['list']);
  }

  async initiateQuickPass(item) {
    console.log(item);

    this.modals.dismiss({
      data: 'A',
    });

    console.log(item)

    const res = await this.modals.present(QuickPassPageComponent, {
      resident_id: this.resident_id,
      display_name: this.display_name,
      dial_code: item.dial_code,
      quick_phone_number: item.formatted_phone_number,
      quick_display_name: item.contact_name,
    });
  }

  async add() {
    const res = await this.modals.present(AddDncComponent, {
      resident_id: this.resident_id,
    });

    this.sync();
    // this.offset = 0;
    // this.search_value = '';
    // this.getAllDNCList(this.search_value, this.offset);
  }

  async deleteItem($event, item) {
    item.delete = true;
    $event.stopPropagation();
    const res = await this.network.deleteContactInDncByGuardByResidentId({
      resident_id: this.resident_id,
      contact_id: item.id,
    });

    this.sync();
  }

  // async presentPopover($event, item) {
  //   const res = await this.popover.present(ContactpopoverPageComponent, {
  //     pid: item,
  //     flag: 'DNC',
  //   });

  //   const data = res.data;

  //   if (data == null) {
  //     return;
  //   }
  //   console.log({ data });
  //   if (data.param == 'edit-dnc') {
  //     const data = await this.edit(item);
  //     const record: any = await this.sqlite.getSingleDncRecord(item.id);
  //     if (record) {
  //       let objIndex = this.plist.findIndex((obj) => obj.id == record.id);
  //       this.plist[objIndex] = record;
  //     }
  //   } else {
  //     const res = await this.removeContactFromList();
  //     if (res) {
  //       console.log({ item });

  //       await this.sqlite.deleteDncContactById(item.id);
  //       setTimeout(() => {
  //         const index = this.plist.indexOf(item);
  //         if (index > -1) {
  //           this.plist.splice(index, 1); // 2nd parameter means remove one item only
  //         }
  //       }, 1000);
  //     }
  //   }
  // }
  // async synContacts() {}

  // edit(res) {
  //   return new Promise(async (resolve) => {
  //     const data = await this.modals.present(AddDncComponent, { dnc_obj: res });
  //     resolve(data);
  //   });
  // }

  // async removeContactFromList() {
  //   return this.utility.presentConfirm(
  //     'Agree',
  //     'Disagree',
  //     'Confirm Delete',
  //     'Do you want to delete this contact from list?'
  //   );
  // }
}
