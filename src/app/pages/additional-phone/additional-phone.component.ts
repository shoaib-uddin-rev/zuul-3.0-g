import {
  AfterViewInit,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-additional-phone',
  templateUrl: './additional-phone.component.html',
  styleUrls: ['./additional-phone.component.scss'],
})
export class AdditionalPhoneComponent extends BasePage implements OnInit {


  @Input() phone1;
  @Input() phone2;
  @Input() phone3;
  phone1_ishidden = false;
  phone2_ishidden = false;
  phone3_ishidden = false;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    if(this.phone1 == ''){
      this.phone1_ishidden = true;
    }
    if(this.phone2 == ''){
      this.phone2_ishidden = true;
    }
    if(this.phone3 == ''){
      this.phone3_ishidden = true;
    }

    console.log(this.phone1,this.phone2,this.phone3);
  }

  callMe(num) {
    console.log(num);
    this.utility.dialMyPhone(num);
  }

  close(res) {
    this.modals.dismiss(res);
  }

}
