import { GuardLicenceImageComponent } from './guard-licence-image/guard-licence-image.component';
import { FirstSlideComponent } from './guard-licence-image/first-slide/first-slide.component';
import { SecondSlideComponent } from './guard-licence-image/second-slide/second-slide.component';
import { TutorialCardsComponentModule } from './../components/tutorial-cards/tutorial-cards-component.module';
import { CommonModule, Location } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { ForgetPasswordPageComponent } from './forget-password-page/forget-password-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { PagesRoutingModule } from './pages-routing.module';
import { SignupComponent } from './signup/signup.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';
import { ContactItemComponent } from '../components/contact-item/contact-item.component';
import { CreateVehiclePageComponent } from './create-vehicle-page/create-vehicle-page.component';
import { ComponentsNotificationItemComponent } from '../components/components-notification-item/components-notification-item.component';
import { ParentalNotificationItemComponent } from '../components/parental-notification-item/parental-notification-item.component';
import { CodeVerificationPageComponent } from './code-verification-page/code-verification-page.component';
import { CountryCodePageComponent } from './country-code-page/country-code-page.component';
import { EmptyviewComponentModule } from '../components/emptyview/emptyview.component.module';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { GuardResidentsPageComponent } from './guard-residents-page/guard-residents-page.component';
import { QuickPassPageComponent } from './quick-pass-page/quick-pass-page.component';
import { GuardLicencePageComponent } from './guard-licence-page/guard-licence-page.component';
import { ContactpopoverPageComponent } from '../components/contactpopover-page/contactpopover-page.component';
import { UpdatePasswordPageComponent } from './update-password-page/update-password-page.component';
import { GuardCodeVerificationPage } from './guard-code-verification/guard-code-verification';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RegisterPageModule } from './register/register.module';
import { CamerasComponent } from './cameras/cameras.component';
import { OpenDoorComponent } from './guard-licence-image/open-door/open-door.component';

@NgModule({
  declarations: [
    SplashPageComponent,
    SignupComponent,
    ForgetPasswordPageComponent,
    DashboardPageComponent,
    LoginPageComponent,
    TutorialPageComponent,
    ContactItemComponent,
    CreateVehiclePageComponent,
    ParentalNotificationItemComponent,
    ComponentsNotificationItemComponent,
    CodeVerificationPageComponent,
    CountryCodePageComponent,
    GuardLicenceImageComponent,
    GuardResidentsPageComponent,
    QuickPassPageComponent,
    GuardLicencePageComponent,
    ContactpopoverPageComponent,
    UpdatePasswordPageComponent,
    CamerasComponent,
    GuardCodeVerificationPage,
    FirstSlideComponent,
    SecondSlideComponent,
    OpenDoorComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TutorialCardsComponentModule,
    EmptyviewComponentModule,
    NgxQRCodeModule,
    Ng2SearchPipeModule,
    RegisterPageModule,
  ],
  exports: [],
  providers: [Location],
})
export class PagesModule {}
