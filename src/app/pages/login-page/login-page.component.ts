/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { PopoverController } from '@ionic/angular';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { BasePage } from '../base-page/base-page';
import { ForgetPasswordPageComponent } from '../forget-password-page/forget-password-page.component';
import { GuardCodeVerificationPage } from '../guard-code-verification/guard-code-verification';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent extends BasePage implements OnInit {
  aForm: FormGroup;
  submitAttempt = false;
  swUuser: any = null;
  @Input() navwithin = false;

  constructor(
    injector: Injector,
    public popoverCtrl: PopoverController,
    public geolocation: GeolocationsService
  ) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {}

  setupForm() {
    //var re = '/\S+@\S+\.\S+/';

    // 4574574578
    // hotmail1VU
    this.aForm = this.formBuilder.group({
      dial_code: ['', Validators.compose([Validators.required])],
      phone_number: [ ''
        ,
        Validators.compose([
          Validators.required,
        ]) /*, VemailValidator.checkEmail */,
      ],
      password: [ ''
        ,
        Validators.compose([
          Validators.minLength(3),
          Validators.maxLength(30),
          Validators.required,
        ]),
      ],
    });
  }

  async openLogUsers(event) {
    const _data = { id: null, flag: 'SW' };
    const data = await this.popover.present(event, _data);

    if (data == null) {
      return;
    }
    const swUser = data.param;
    this.setUserInForm(swUser);
  }

  setUserInForm(swUser) {
    if (swUser) {
      this.users.switchUserAccount(swUser).then((data) => {
        if (!data) {
          // this.navwithin = true;
          this.aForm.controls.phone_number.setValue(swUser.phone_number);
        }
      });
    }
  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType !== 'deleteContentBackward') {

      let dial_code = this.aForm.controls['dial_code'].value;
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(
        ev.target.value,
        true,
        dial_code
      );
      ev.target.value = utel;
      this.aForm.controls['phone_number'].patchValue(utel);

      // const utel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      // console.log(utel);
      // // this.aForm.controls["phone_number"].setValue(_tel);
      // ev.target.value = utel;
      // this.aForm.controls.phone_number.setValue(utel);
    }
  }

  forgetpassword() {
    this.modals.present(ForgetPasswordPageComponent);
  }

  async login() {
    this.submitAttempt = true;
    let formdata = this.aForm.value;

    formdata.register_with_phonenumber = true;
    // const isWithinLocation = await this.geolocation.geoFencing();
    // if (isWithinLocation) {
    formdata.phone_number = this.utility.getOnlyDigits(formdata.phone_number);
    formdata.dial_code = formdata.dial_code == 'HN' ? '+504' : '+1'
    this.users.login(formdata);

    // } else {
    // this.utility.showAlert("You should be within 100ft of your assigned Location to login !", "Out Of Range");
    // }
    // this.events.publish('user:login', formdata);
  }

  signup() {
    this.nav.setRoot('pages/SignupPage', {
      animate: true,
      direction: 'forward',
    });
  }

  redirectTo() {
    this.modals.present(GuardCodeVerificationPage);
  }
  onCountryCodeChange($event) {
    console.log($event);
  }
}

