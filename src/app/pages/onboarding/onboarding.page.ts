import { Component, Injector, OnInit } from '@angular/core';
import { GmapComponent } from 'src/app/components/gmap/gmap.component';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { BasePage } from '../base-page/base-page';
import { CamerasComponent } from '../cameras/cameras.component';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage extends BasePage implements OnInit {
  slide = 1;
  loading = false;
  rover_mode = false;
  account_type = 'standard';
  rover_type = 'standard_rover';
  _location: any;
  default_cam: any;
  air_print = false;
  isLoadingCam = false;
  camerasAvailable = true;
  remote_guard = false;
  front_camera: any;
  doorbird_cam: any;
  doorbird_enable = false;
  doorBirdAvailable = true;

  constructor(injector: Injector, private geolocation: GeolocationsService) {
    super(injector);
  }

  ngOnInit() {}

  async getflags() {
    this.isLoadingCam = true;
    const user = await this.users.getCurrentUser();
    const default_cam_id = user.default_camera;
    // const latlng = await this.geolocation.getCurrentLocationCoordinates();

    const camera_count = await this.network.getCameraCount();

    console.log(camera_count);
    var doorbird = camera_count['doorbird'];
    var lpr = camera_count['lpr'];
    var non_lpr = camera_count['non-lpr'];

    this.doorBirdAvailable = doorbird > 0;

    // let params = latlng;
    // params['type'] = 'doorbird';
    // const doorbirdcams = await this.network.getCameras(params);
    // console.log(doorbirdcams, 'Door Bird ');

    // if (!doorbirdcams || doorbirdcams.length == 0) {
    //   this.doorBirdAvailable = false;
    // }
    // if (!data || data.lenght == 0) {
    this.camerasAvailable = lpr + non_lpr > 0;
    // }

    // let default_cam = data.find((x) => x.id == default_cam_id);

    this.isLoadingCam = false;
    // if (default_cam) {
    //   this.default_cam = default_cam;
    // }
  }

  close() {
    this.modals.dismiss();
  }

  next() {
    console.log(this.slide);
    if (this.slide == 3) {
      this.getflags();
    }

    if (this.slide < 4) {
      this.slide++;
    }
  }

  back() {
    console.log(this.slide);

    if (this.slide > 1) {
      this.slide--;
    }
  }

  async done() {
    const data = {
      lat: this._location.lat,
      long: this._location.lng,
    };

    const res = await this.network.setGuardAssignLocation(data);
    console.log(res);

    const obj = {
      rover_mode: this.rover_mode == true ? '1' : '0',
      remote_guard: this.remote_guard == true ? '1' : '0',
      rover_type: this.rover_type,
      default_camera: this.default_cam ? this.default_cam.id.toString() : null,
      // camera_id: this.front_camera ? this.front_camera.id.toString() : null,
      doorbird_cam_id: this.doorbird_cam ? this.doorbird_cam.id : null,
      air_print: this.air_print.toString(),
      is_onboarded: '1',
    };

    console.log(obj);
    const update = await this.network.updateGuardProfile(obj);
    console.log(update);
    // console.log(this.account_type);

    await this.utility.showAlert('Your settings have been set', 'Success');
    // localStorage.setItem('user:onboarded', true.toString());

    this.close();
  }

  async setLocation(location) {
    // console.log(location);
    this._location = location;
  }

  async openMap() {


    if(Capacitor.getPlatform() == 'web'){
      let loc = { lat: 24.929631566724076, lng: 67.11523365530714, address: 'Block 1 Gulistan-e-Johar, Karachi, 72500', parts: [] };
      this.setLocation(loc);
      return;
    }



    const { data } = await this.modals.present(GmapComponent);
    if (data.lat || data.lng) {
      console.log(data);
      this.setLocation(data);
    } else {
      this.utility.showAlert('Please Select Your Location Again', 'Failed');
    }
  }

  async openCamerasModal(type = 'default') {
    const { data } = await this.modals.present(CamerasComponent, {
      type: type,
    });
    if (data.camera) {
      // if (type == 'front_camera') {
      //   this.front_camera = data.camera;
      // } else

      if (type == 'doorbird') {
        this.doorbird_cam = data.camera;
      } else {
        this.default_cam = data.camera;
      }
    }
  }
}
