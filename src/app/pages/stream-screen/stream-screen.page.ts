import {
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { DomSanitizer } from '@angular/platform-browser';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-stream-screen',
  templateUrl: './stream-screen.page.html',
  styleUrls: ['./stream-screen.page.scss'],
})
export class StreamScreenPage extends BasePage implements OnInit {
  @ViewChild('iframe') iframe: ElementRef;
  guard_id: any;
  camera_id: any;
  stream: any;
  isLoading = true;
  contentWidth: any;
  contentHeight: any;
  scan_log: any;
  type: any;

  constructor(injector: Injector, public domSanitizer: DomSanitizer) {
    super(injector);
  }

  ngOnInit() {
    setTimeout(async () => {
      this.contentWidth = await this.iframe.nativeElement.offsetWidth;
      this.contentHeight = (this.contentWidth * 3) / 4;
    }, 1000);
    this.doRefresh();
  }

  async ngAfterViewInit() {}

  doRefresh() {
    if (this.type == 'doorbird') {
      this.getDoorbirdCameraImage();
    } else {
      this.getCameraStream();
    }
  }

  timeintervel = null;
  timer = 7;
  async getDoorbirdCameraImage() {
    const res = await this.network.setDoorbirdCameraToInitiate();

    this.timeintervel = setInterval(() => {
      console.log('timer');
      this.timer = this.timer - 1;
      if ((this.timer = 0)) {
        clearInterval(this.timeintervel);
      }
    }, 2000);
  }

  async getCameraStream() {
    let data = {
      guard_id: this.guard_id,
      camera_id: this.camera_id,
    };
    console.log(data);
    var st = await this.network.getCameraStream(data);
    st.url = this.domSanitizer.bypassSecurityTrustResourceUrl(st.url);
    this.stream = st;

    console.log(this.stream);

    this.isLoading = false;
  }

  closeModal(data) {
    this.modals.dismiss(data);
  }

  async snapImage() {
    let params = {
      scanlog_id: this.scan_log.scan_log.id,
      camera_id: 13,
    };
    let res;
    if (this.type == 'doorbird') {
      res = await this.network.snapCameraImage(this.camera_id);
    } else if (this.type == 'lpr') {
      res = await this.network.captureStreamImage(params);
    }

    let license_image_url = res.url;
    let data = {
      license_image_url: license_image_url,
    };
    this.closeModal(data);
  }
}
