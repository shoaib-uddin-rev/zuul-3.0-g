import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StreamScreenPageRoutingModule } from './stream-screen-routing.module';

import { StreamScreenPage } from './stream-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StreamScreenPageRoutingModule
  ],
  declarations: [StreamScreenPage]
})
export class StreamScreenPageModule {}
