import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RingCentralPageRoutingModule } from './ring-central-routing.module';

import { RingCentralPage } from './ring-central.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RingCentralPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RingCentralPage]
})
export class RingCentralPageModule {}
