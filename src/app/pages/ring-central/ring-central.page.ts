import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { RingCentral } from '../../services/ringcentral.service';
import { AudioService } from 'src/app/services/audio.service';

@Component({
  selector: 'app-ring-central',
  templateUrl: './ring-central.page.html',
  styleUrls: ['./ring-central.page.scss'],
})
export class RingCentralPage extends BasePage implements OnInit {

  // aForm: FormGroup;
  // submitAttempt = false;
  token: any;
  msgList: any;
  isPlaying = false;
  playing_index;

  constructor(
    injector: Injector,
    private ringCentral: RingCentral,
    private audio: AudioService
  ) { 
    super(injector)
    // this.setupForm();
  }

  ngOnInit() {
    this.getCallLogs();
    this.getMessageList();
  }

  // setupForm() {

  //   this.aForm = this.formBuilder.group({
  //     phone: ['021231', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
  //     password: ['asd4as56d4', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])],
  //     ext: [101, Validators.compose([Validators.minLength(3), Validators.required])]
  //   });

  // }

  // onTelephoneChange(ev, tel) {
  //   if (ev.inputType !== 'deleteContentBackward') {
  //     const utel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
  //     console.log(utel);
  //     ev.target.value = utel;
  //     this.aForm.controls.phone.setValue(utel);
  //   }
  // }

  // async login() {
  //   const formdata = this.aForm.value; 
  //   formdata.phone = '1' + formdata.phone;
  //   const res = await this.ringCentral.getToken(formdata);
  //   console.log(res);
  //   this.modals.dismiss();
  // }
  async getCallLogs() {
    let logs = await this.ringCentral.getCallLogs();
    console.log(logs);
  }

  async getMessageList() {
    let msgs = await this.ringCentral.getMessageList();
    this.msgList = msgs['data'].records;
    console.log("Messages" , this.msgList);
    this.token = msgs['token'];
  }

  close() {
    this.modals.dismiss();
  }

  playAudio(recording) {
    const uri = recording.uri;
    recording.playing_index = true;
    if (!this.isPlaying) {
      let url = uri + '?access_token=' + this.token;
      this.audio.playRecording(url);
    } else {
      recording.playing_index = false;
      this.audio.pauseRecording();
    }

    this.isPlaying = !this.isPlaying;

  }
}
