import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RingCentralPage } from './ring-central.page';

const routes: Routes = [
  {
    path: '',
    component: RingCentralPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RingCentralPageRoutingModule {}
