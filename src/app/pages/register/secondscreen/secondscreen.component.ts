import { StateService } from './../../../services/state.service';
import { FormGroup, Validators } from '@angular/forms';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-secondscreen',
  templateUrl: './secondscreen.component.html',
  styleUrls: ['./secondscreen.component.scss'],
})
export class SecondscreenComponent extends BasePage implements OnInit {

  @Input() dial_code: any = {
    name: 'United States',
    dial_code: '+1',
    code: 'US',
    image: 'assets/imgs/flags/us.png',
  };;
  aForm: FormGroup;
  profile: any;
  tuser: any;
  get user(): any {
    return this.tuser;
  }
  @Input() set user(value: any) {
    this.tuser = value;

    // console.log(value);
    this.fetchProfileValues();
  };



  states: any = [];
  constructor(injector: Injector, public statesService: StateService) {
    super(injector);
    this.setupForm();
    this.states = statesService.getStates();

    this.events.subscribe('registration:secondScreenValidation', this.secondScreenValidation.bind(this));
  }

  ngOnInit() { }

  setupForm() {

    this.aForm = this.formBuilder.group({

      apartment: [''],
      street_address: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      state: ['', Validators.compose([Validators.required])],
      zip_code: [
        '',
        Validators.compose([Validators.required, Validators.pattern('[0-9]*')]),
      ],
    });
  }

  async fetchProfileValues() {
    // get user data

    if (!this.user) {
      return;
    }

    // redirect to welcomeid
    this.profile = this.user;
    // console.log(this.profile);

    this.aForm.controls.apartment.setValue(this.profile.apartment);
    this.aForm.controls.street_address.setValue(
      this.profile.street_address
    );
    this.aForm.controls.city.setValue(this.profile.city);
    this.aForm.controls.state.setValue(this.profile.state);
    this.aForm.controls.zip_code.setValue(this.profile.zip_code);

  }

  onZipCodeKeyUp(event) {
    // console.log(event.target.value);
    let e = event.target.value;
    e = e.replace(/\D/g, '');
    // console.log(e);
    event.target.value = e;
  }


  // async goToNext(num) {

  //   const flag = await this.validateForm();
  //   console.log(flag);
  //   if (!flag) {
  //     return;
  //   }

  //   this.events.publish('registration:setdata', this.aForm.value);
  //   this.events.publish('registration:slidetoNext', num);

  // }

  async secondScreenValidation(){
    const flag = await this.validateForm();
    // console.log(flag);

    if(flag === true){
      this.events.publish('registration:secondScreenValidated', this.aForm.value);
    }
  }

  validateForm(): Promise<boolean> {

    const eror = 'Please Enter Valid';

    return new Promise( resolve => {

      const invalidApartment = !this.aForm.controls.apartment.valid;
      const invalidVaddress = !this.aForm.controls.street_address.valid;
      const invalidCity = !this.aForm.controls.city.valid;
      const invalidState = !this.aForm.controls.state.valid;
      const invalidZipcode = !this.aForm.controls.zip_code.valid;

      if (invalidApartment) {
        this.utility.presentFailureToast(eror + ' appartment');
        resolve(false);
        return;
      }
      // if(invalidVaddress){ this.showAlert(eror + " address"); return }
      if (invalidCity) {
        this.utility.presentFailureToast(eror + ' city');
        resolve(false);
        return;
      }
      if (invalidState) {
        this.utility.presentFailureToast(eror + ' state');
        resolve(false);
        return;
      }
      if (invalidZipcode) {
        this.utility.presentFailureToast(eror + ' zip code');
        resolve(false);
        return;
      }

      resolve(true);





    });

  }

}
