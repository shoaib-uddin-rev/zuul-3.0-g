import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';
import { WelcomescreenComponent } from './welcomescreen/welcomescreen.component';
import { FirstscreenComponent } from './firstscreen/firstscreen.component';
import { SecondscreenComponent } from './secondscreen/secondscreen.component';
import { ThirdscreenComponent } from './thirdscreen/thirdscreen.component';
import { SuccessscreenComponent } from './successscreen/successscreen.component';
import { WizardComponent } from './wizard/wizard.component';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RegisterPageRoutingModule,
    EmptyviewComponentModule
  ],
  declarations: [
    RegisterPage,
    WelcomescreenComponent,
    FirstscreenComponent,
    SecondscreenComponent,
    ThirdscreenComponent,
    SuccessscreenComponent,
    WizardComponent
  ],
})
export class RegisterPageModule {}
