import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Error404Page } from './error404';

@NgModule({
  declarations: [
    Error404Page,
  ],
  imports: [
    IonicPageModule.forChild(Error404Page),
  ],
})
export class Error404PageModule {}
