import { Component } from '@angular/core';;
import { EventsService } from 'src/app/services/events.service';

/**
 * Generated class for the Error404Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'app-page-error404',
  templateUrl: 'error404.html',
})
export class Error404Page {

  constructor(
    private events: EventsService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Error404Page');
  }

  clearStorage(){
    this.events.publish('user:logout');
  }

}
function IonicPage() {
  throw new Error('Function not implemented.');
}

