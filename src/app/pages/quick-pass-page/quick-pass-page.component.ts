import {
  Component,
  OnInit,
  AfterViewInit,
  Injector,
  Input,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { GuardLicenceImageComponent } from '../guard-licence-image/guard-licence-image.component';

@Component({
  selector: 'app-quick-pass-page',
  templateUrl: './quick-pass-page.component.html',
  styleUrls: ['./quick-pass-page.component.scss'],
})
export class QuickPassPageComponent
  extends BasePage
  implements OnInit, AfterViewInit
{
  @Input() resident_id;
  @Input() display_name;
  guard_name;
  user;
  aForm: FormGroup;

  @Input() quick_phone_number;
  @Input() quick_display_name;
  @Input() dial_code;

  constructor(injector: Injector) {
    super(injector);
    this.initialize();
    this.setupForm();
  }

  ngOnInit() {}

  ngAfterViewInit() {
    // console.log(this.aForm);
    if (this.quick_phone_number) {
      this.aForm.controls['phone_number'].setValue(this.quick_phone_number);
      this.aForm.controls['visitor_type'].setValue('1');
    }

    if (this.quick_display_name) {
      this.aForm.controls['display_name'].setValue(this.quick_display_name);
    }

    setTimeout(async () => {
      this.user = await this.sqlite.getActiveUser();
      console.log(this.user['dial_code']);

      if(!this.dial_code){
        this.dial_code = (this.user['dial_code'] == '+504') ? 'HN' : 'US'
        this.aForm.controls['dial_code'].setValue(
          this.dial_code
        );
      } else {
        let dc = this.dial_code && this.dial_code == '+504' ? 'HN' : 'US'
        this.aForm.controls['dial_code'].setValue(
          dc
        );
      }

    }, 1000);
  }

  knownFilterForGuard(list: any[]) {
    return list.filter((x) => {
      let a = this.utility.getOnlyDigits(x.dial_code ?? '');
      let b = this.utility.getOnlyDigits(
        this.user ? this.user['dial_code'] : ''
      );

      return a == b;
    });
  }

  async initialize() {
    this.user = await this.sqlite.getActiveUser(); //JSON.parse(localStorage.getItem('user')); // await this.sqlite.getActiveUser();
    console.log(this.user);
    this.guard_name = this.user.name;
  }

  async setupForm() {
    // shoaib uddin
    // 3432322008
    this.aForm = this.formBuilder.group({
      dial_code: ['US', Validators.compose([Validators.required])],
      display_name: ['', Validators.compose([Validators.required])],
      phone_number: ['', Validators.compose([])],
      visitor_type: ['1', Validators.compose([Validators.required])],
    });
  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType != 'deleteContentBackward') {
      let dial_code = this.aForm.controls['dial_code'].value;
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(
        ev.target.value,
        true,
        dial_code
      );
      ev.target.value = utel;
      this.aForm.controls['phone_number'].patchValue(utel);

      // const utel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      // ev.target.value = utel;
      // this.aForm.controls['phone_number'].setValue(utel);
    }
  }

  validate() {
    var in_name =
      !this.aForm.controls.display_name.valid &&
      !this.utility.isLastNameExist(this.aForm.controls.display_name.value);

    var in_phone = !this.aForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(
      this.aForm.controls.phone_number.value
    );

    if (in_name) {
      this.utility.presentFailureToast('Name/Full Name is required');
      return false;
    }

    // if (in_phone) {
    //   this.utility.presentFailureToast('Phone Number required');
    //   return false;
    // }

    return true;
  }

  send(addVehicle = false) {
    if (!this.validate()) {
      return;
    }

    var formdata = this.aForm.value;
    // console.log(formdata);
    formdata['dial_code'] =
      this.aForm.controls.dial_code.value == 'HN' ? '+504' : '+1';
    formdata['resident_id'] = this.resident_id;
    formdata['phone_number'] = this.aForm.controls.phone_number.value
      ? this.utility.getOnlyDigits(this.aForm.controls.phone_number.value)
      : '1111111111';

    this.network.quickPassViaGuard(formdata).then((res) => {
      console.log(res);
      this.modals.dismiss().then(async () => {
        let alertmode = res['alert_mode'];
        // console.log(alertmode);
        if (alertmode['flag'] == true) {
          await this.utility.showAlert(alertmode['message']);
        }

        var data = res;
        data['add_vehicle'] = res['added_user']['vehicle'];
        // console.log(data);
        this.gotoGuardLicenceImage(data);
      });
    });
  }

  gotoGuardLicenceImage(data) {
    
    // this.modals.present(GuardLicenceImageComponent, { data });
    this.nav.push('GuardLicenceImage', { res: JSON.stringify(data) });
  }

  closeModel() {
    this.modals.dismiss();
  }

  onCountryCodeChange($event) {
    console.log($event);
  }
}
