import { Component, OnInit, Injector } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MenuController, Platform } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { UtilityService } from 'src/app/services/utility.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.scss'],
})
export class SplashPageComponent extends BasePage implements OnInit {
  tapCount = 0;
  skipintro = false;
  stimeout;
  isSync = false;
  aftertimeout = true;

  constructor(
    injector: Injector,
    private splashScreen: SplashScreen,
    private menu: MenuController
  ) {
    super(injector);
    this.platform.ready().then(() => {
      this.splashScreen.hide();
    });
    this.menu.swipeGesture(false);
    this.init();
  }
  ngOnInit(): void {}

  async init() {
    // get all data from local storage

    const self = this;
    this.stimeout = setTimeout(() => {
      self.callRedirect();
      self.aftertimeout = false;
    }, 1300);
  }

  async callRedirect() {
    // temporary waiting
    // const token = await this.sqlite.getCurrentUserAuthorizationToken();
    this.events.publish('user:get');
  }

  ionViewDidLeave() {
    this.menu.swipeGesture(true);
  }
}
