import { QuickPassPageComponent } from './../quick-pass-page/quick-pass-page.component';
import {
  Component,
  Injector,
  OnInit,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { RingCentralPage } from '../ring-central/ring-central.page';
import { ResidentsService } from 'src/app/services/residents.service';
import { DoNotCallListPage } from '../do-not-call-list/do-not-call-list.page';
import { AddDncComponent } from '../do-not-call-list/add-dnc/add-dnc.component';
import { AdditionalPhoneComponent } from '../additional-phone/additional-phone.component';
import { OpenDoorComponent } from '../guard-licence-image/open-door/open-door.component';

@Component({
  selector: 'app-guard-residents-page',
  templateUrl: './guard-residents-page.component.html',
  styleUrls: ['./guard-residents-page.component.scss'],
})
export class GuardResidentsPageComponent
  extends BasePage
  implements OnInit, AfterViewInit, OnDestroy
{
  avatar = this.users.avatar;
  storedData: any;
  isIOSPlatform: boolean = true;
  searchTerm: string = '';
  page = 1;
  isWithinLocation: any = true;
  isLoading = false;
  isRingCentral: boolean;
  isSync = false;
  offset = 0;
  additional_phone_data

  constructor(
    injector: Injector,
    private geolocation: GeolocationsService,
    public residents: ResidentsService
  ) {
    super(injector);
    this.init();
  }

  async ngOnInit() {
    this.residents.getAllResidents(this.searchTerm, this.offset);
  }

  async init() {
    this.isWithinLocation = true; // await this.geolocation.geoFencing();
    const user = this.users._user;
    this.isRingCentral = user['is_ringcentral'] == 1 ? true : false;
  }

  ngAfterViewInit() {
    this.fetchProfileValues();
    this.isIOSPlatform = this.platform.is('ios');
  }

  async fetchProfileValues() {
    // this.getGuardResidents('');
  }

  formatPhoneNumber(num) {
    return this.utility.formatPhoneNumberRuntime(num);
  }

  callMe(num) {
    console.log("This button click")
    var format_phone = ""
    if(num['detail']['value'].length != 14){
      format_phone = "("+num['detail']['value'][0]+num['detail']['value'][1]+num['detail']['value'][2]+") "+num['detail']['value'][3]+num['detail']['value'][4]+num['detail']['value'][5]+"-"+num['detail']['value'][6]+num['detail']['value'][7]+num['detail']['value'][8]+num['detail']['value'][9];
    }else{
      format_phone = num['detail']['value']
    }

    console.log(format_phone)
    this.utility.dialMyPhone(format_phone);
  }

  async quickpassViaAdminGuard(item) {
    // quick pass params
    // guard_id: get guard id from server bu auth
    // resident_id: item.id
    // Full name of person
    // phone_number
    if (this.isWithinLocation) {
      this.modals.present(QuickPassPageComponent, {
        resident_id: item.id,
        display_name: item.first_name + ' ' + item.last_name,
      });
    } else {
      this.utility.showAlert(
        'You should be within 100ft of your assigned Location to Quick Pass !',
        'Out Of Range'
      );
    }
  }

  async additionalPhoneNumbers(phone1,phone2,phone3) {

      this.modals.present(AdditionalPhoneComponent, {
        phone1: phone1,
        phone2: phone2,
        phone3: phone3,
      });

  }

  async dncViaAdminGuard(item) {
    // quick pass params
    // guard_id: get guard id from server bu auth
    // resident_id: item.id
    // Full name of person
    // phone_number
    if (this.isWithinLocation) {
      this.modals.present(DoNotCallListPage, {
        resident_id: item.id,
        display_name: item.first_name + ' ' + item.last_name,
      });
    } else {
      this.utility.showAlert(
        'You should be within 100ft of your assigned Location to Quick Pass !',
        'Out Of Range'
      );
    }
  }

  async loadMore($event) {
    this.offset = this.residents.offset;
    await this.residents.getAllResidents(this.searchTerm, this.offset);
    $event.target.complete();
  }

  timer;
  clearUpdate($event) {
    this.residents.offset = 0;
    this.offset = 0;
    this.searchTerm = '';
    clearTimeout(this.timer);
    // this.timer = setTimeout(() => {
    this.residents.getAllResidents(this.searchTerm, this.offset);
  }

  searchUpdate($event) {
    const v = $event.target.value;
    this.residents.offset = 0;
    this.offset = 0;
    this.searchTerm = v;
    clearTimeout(this.timer);
    // this.timer = setTimeout(() => {
    this.residents.getAllResidents(this.searchTerm, this.offset);
    // }, 700);
  }

  getGuardResidents(search) {
    return new Promise<void>((resolve) => {
      let params = {
        page: 1,
        search: search,
      };

      this.residents.getAllResidents(this.searchTerm, this.offset);
    });
  }

  closeModel() {
    this.nav.pop();
  }

  // setFilteredItems($event, flag) {
  //   this.searchTerm = $event.target.value;
  //   this.getGuardResidents();
  // }

  openCallLogs() {
    this.modals.present(RingCentralPage);
  }

  ngOnDestroy() {
    this.residents.allResidents = [];
    this.offset = 0;
  }

  async sync() {
    this.isSync = true;
    this.isLoading = true;

    let params = {
      page: -1,
      search: '',
    };

    this.residents.allResidents = [];
    let id = await this.sqlite.getActiveUserId();

    await this.network.getGuardResidents(params).then(async (result) => {
      await this.sqlite.setResidentsInDatabase(
        result['list'],
        id
      );

      this.searchTerm = '';
      this.offset = 0;

      await this.residents.getAllResidents(this.searchTerm, this.offset);

      this.isSync = false;
      this.isLoading = false;
    });

    // await this.network.getGuardResidents(params).then(async (result) => {
    //   this.searchTerm = '';
    //   this.offset = 0;
    //   this.residents.allResidents = result['list'];
    //   this.isSync = false;
    //   this.isLoading = false;
    // });

  }

  async add() {
    this.searchTerm = '';
    this.offset = 0;
    const res = await this.modals.present(AddDncComponent);
    this.residents.getAllResidents(this.searchTerm, this.offset);
    // this.offset = 0;
    // this.search_value = '';
    // this.getAllDNCList(this.search_value, this.offset);
  }

  returnPhoneFormat(c){
    // console.log(c)
    // let f = this.fphs()
    return c.formatted_phone
  }
}
