import {
  AfterViewInit,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { BasePage } from '../../base-page/base-page';
import { AddCategoryComponent } from '../add-category/add-category.component';

@Component({
  selector: 'app-add-dnc',
  templateUrl: './add-dnc.component.html',
  styleUrls: ['./add-dnc.component.scss'],
})
export class AddDncComponent extends BasePage implements OnInit, AfterViewInit {
  @Input() dnc_obj: any;

  @Input() dial_code = {
    name: 'United States',
    dial_code: '+1',
    code: 'US',
    image: 'assets/imgs/flags/us.png',
  };
  aForm: FormGroup;
  submitAttempt = false;
  loading = false;
  dnc_categories: any = [];

  btnText = 'Create';
  contact_id: any;
  vendor_contacts = [];

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
  }

  async ngOnInit() {
    // await this.getCategories();
    this.initializeValues();

    // if (this.isFromRequestPassScreen == true) {
    //   this.btnText = 'Request';
    // }
  }

  ngAfterViewInit() {}

  initializeValues() {
    if (this.dnc_obj) {
      console.log('DNC OBJ: ', this.dnc_obj);

      this.aForm.controls['vendor_name'].setValue(this.dnc_obj.vendor_name);
      this.aForm.controls['vendor_contacts'].setValue(
        this.dnc_obj.vendor_contacts
      );
      this.aForm.controls['dial_code'].setValue(this.dnc_obj.dial_code);
      this.aForm.controls['is_private_household'].setValue(
        this.dnc_obj.is_private_household
      );
    }
  }

  async getCategories() {
    this.dnc_categories = await this.sqlite.getDncCategories();
    console.log('Dnc cat: ', this.dnc_categories);
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      vendor_name: ['', Validators.compose([Validators.required])],
      is_private_household: [true],
      dial_code: ['+1'],
      vendor_contacts: [''],
    });
  }

  close(res) {
    this.modals.dismiss(res);
  }

  create() {
    // add validations
    const vendName = this.aForm.controls.vendor_name.value;
    if (!vendName) {
      this.utility.presentFailureToast('Please type vendor name');
      return;
    }
    this.loading = true;
    this.createContact().then(
      (data) => {
        this.loading = false;
        this.close(data);
      },
      (err) => {
        this.loading = false;

        // console.log(err);
      }
    );
  }

  async createContact() {
    const user_id = await this.sqlite.getActiveUserId();

    return new Promise((resolve, reject) => {
      this.submitAttempt = true;
      let formdata = this.aForm.value;
      formdata['user_id'] = user_id;

      console.log({ formdata });
      if (this.dnc_obj) {
        formdata['id'] = this.dnc_obj.id;
        this.loading = true;
        let data = [];
        data.push(formdata);

        this.sqlite.setDoNotCallListInDatabase(data).then(
          (res: any) => {
            this.loading = false;
            console.log({ res });
            resolve(res);
          },
          (err) => {
            alert('update error');
            this.loading = false;

            reject(err);
          }
        );
      } else {
        this.loading = true;
        let data = [];
        data.push(formdata);
        console.log({ data });
        this.sqlite.setDoNotCallListInDatabase(data).then(
          (res: any) => {
            this.loading = false;
            console.log({ res });
            // this.utility.presentSuccessToast(res.message);
            resolve(res);
          },
          (err) => {
            this.loading = false;
            reject(err);
          }
        );
      }

      // console.log(formdata);
    });
  }

  async openCounryCode() {
    // CreateGroupPage as modal
    const res = await this.modals.present(CountryCodeComponent, {
      dc: this.dial_code,
    });
    const data = res.data;
    if (data.data != 'A') {
      this.dial_code = data;
      this.aForm.controls.dial_code.setValue(this.dial_code.dial_code);
    }
  }

  onTelephoneChange(ev) {
    if (ev.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(
        ev.target.value,
        false
      );
      console.log(utel);
      ev.target.value = utel;
      this.aForm.controls['vendor_contacts'].patchValue(utel);
      // ev.target.value = utel;
    }
  }

  addPhoneNumbers() {}

  async addNewCategory(val: Event) {
    const res = await this.modals.present(AddCategoryComponent);
    console.log(res.data);
    await this.getCategories();
  }

  removeContacts(item) {
    this.vendor_contacts = this.vendor_contacts.filter((x) => x != item);
  }
}
