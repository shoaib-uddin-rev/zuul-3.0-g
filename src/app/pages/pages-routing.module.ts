import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { Error404Page } from './error404/error404';
import { GuardLicenceImageComponent } from './guard-licence-image/guard-licence-image.component';
import { GuardResidentsPageComponent } from './guard-residents-page/guard-residents-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SignupComponent } from './signup/signup.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';

const routes: Routes = [
  { path: 'splash', component: SplashPageComponent },
  { path: 'TutorialPage', component: TutorialPageComponent },
  { path: 'LoginPage', component: LoginPageComponent },
  { path: 'SignupPage', component: SignupComponent },
  { path: 'dashboard', component: DashboardPageComponent },
  { path: 'GuardResidentsPage', component: GuardResidentsPageComponent },
  { path: 'GuardLicenceImage', component: GuardLicenceImageComponent },
  { path: 'Error404Page', component: Error404Page },
  {
    path: 'settings',
    loadChildren: () =>
      import('./settings/settings.module').then((m) => m.SettingsPageModule),
  },
  {
    path: 'ring-central',
    loadChildren: () =>
      import('./ring-central/ring-central.module').then(
        (m) => m.RingCentralPageModule
      ),
  },
  {
    path: 'onboarding',
    loadChildren: () =>
      import('./onboarding/onboarding.module').then(
        (m) => m.OnboardingPageModule
      ),
  },
  {
    path: 'stream-screen',
    loadChildren: () =>
      import('./stream-screen/stream-screen.module').then(
        (m) => m.StreamScreenPageModule
      ),
  },
  {
    path: 'do-not-call-list',
    loadChildren: () =>
      import('./do-not-call-list/do-not-call-list.module').then(
        (m) => m.DoNotCallListPageModule
      ),
  },
  // {
  //     path: '',
  //     redirectTo: 'splash',
  //     pathMatch: 'full'
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
