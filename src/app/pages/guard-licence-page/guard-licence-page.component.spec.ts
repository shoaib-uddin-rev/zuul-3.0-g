import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GuardLicencePageComponent } from './guard-licence-page.component';

describe('GuardLicencePageComponent', () => {
  let component: GuardLicencePageComponent;
  let fixture: ComponentFixture<GuardLicencePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardLicencePageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GuardLicencePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
