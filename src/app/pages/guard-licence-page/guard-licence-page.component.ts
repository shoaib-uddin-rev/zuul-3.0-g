import { Component, OnInit, AfterViewInit, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { DomSanitizer } from '@angular/platform-browser';

const userdata = require('src/app/data/user.json');
@Component({
  selector: 'app-guard-licence-page',
  templateUrl: './guard-licence-page.component.html',
  styleUrls: ['./guard-licence-page.component.scss'],
})
export class GuardLicencePageComponent extends BasePage implements OnInit, AfterViewInit {

  aForm: FormGroup;
  submitAttempt = false;
  liImageBase64 = '';
  profile: any;
  user: any;
  data: any;
  tempaccesstoken: any;
  date_of_birth_mdy = '';
  revisit = false;
  license_image: any;

  constructor(injector: Injector, public domSanitizer: DomSanitizer,) {
    super(injector);
    this.initialize();
  }

  ngOnInit() { }

  ngAfterViewInit() { }

  async initialize() {
    this.user = JSON.parse(localStorage.getItem('user')) // await this.sqlite.getActiveUser();
    console.log(this.user);
  }

  getlicenseimage() {

    if (!this.platform.is('cordova')) {
      this.liImageBase64 = userdata.small_image as string;;
      this.license_image = this.liImageBase64;
    } else {
      this.utility.snapImage('license').then(image => {
        this.liImageBase64 = image as string;
        this.license_image = this.liImageBase64;
      });
    }



  }

  async finishregister() {

    const in_license_image = !this.license_image;

    const eror = 'Please enter valid';
    if (in_license_image) { this.utility.showAlert('Please Upload License Image'); return; }

    this.submitAttempt = true;
    let formdata = {
      license_image: this.license_image,
    };
    formdata["id"] = this.data.user_id;

    // this.utility.showLoader();

    console.log(this.data);

    const image = this.platform.is('cordova') ?
      await this.utility.convertImageUrltoBase64(formdata.license_image) : formdata.license_image.replace('data:image/png;base64,', '');

    formdata.license_image = image;
    formdata["license_via_guard"] = true,
      formdata["scanlog_id"] = this.user.scanlog_id;
    console.log('scanlog_id', formdata["scanlog_id"]);
    formdata["license_type"] = "driving_license";

    // this.utility.hideLoader();

    this.network.updateProfileById(formdata["id"], formdata).then(async (res) => {

      await this.utility.showAlert('License Image uploaded, thank you for cooperation', 'Upload Successful');
      this.closeDismiss(formdata);

    });
    // });



  }

  closeDismiss(res) {
    this.modals.dismiss(res);
  }



}
