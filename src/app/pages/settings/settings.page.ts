import { Component, Injector, OnInit } from '@angular/core';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { BasePage } from '../base-page/base-page';
import { CamerasComponent } from '../cameras/cameras.component';
import { WebRelayComponent } from './web-relay/web-relay.component';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage extends BasePage implements OnInit {
  user: any;
  loading = false;
  rover_mode: any = false;
  enable_remote_guard: any = false;
  default_cam: any;
  ring_central_token: any;
  rover_type: any;
  is_onboarded: any;
  air_print: any;
  camerasAvailable = true;
  frontCamerasAvailable = true;
  doorbirdCamerasAvailable = true;
  front_camera: any;
  doorbird_camera: any;
  enableDoorbird = false;
  showDoorbird = false;
  webRelay: any;
  web_relays: Array<any>;
  check_toggle: boolean = false;
  community_id = null;
  constructor(injector: Injector, private geolocation: GeolocationsService) {
    super(injector);
  }

  async ngOnInit() {
    await this.fetchUserFlags();
  }

  ionViewWillEnter() {
    var sa = localStorage.getItem('check_toggle');
    if (sa == 'true'){
      this.check_toggle = true;
    }else{
      this.check_toggle = false
    }
    console.log('safdf', sa);

    var edb = localStorage.getItem('enabledoorbird');
    if (edb == 'true') {
      this.enableDoorbird = true;
    } else {
      this.enableDoorbird = false;
    }
  }

  async fetchUserFlags() {
    this.loading = true;
    this.rover_mode = this.users._user.rover_mode == '0' ? false : true;
    this.rover_type = this.users._user.rover_type;
    this.is_onboarded = this.users._user.is_onboarded == '0' ? false : true;
    this.enable_remote_guard = this.users._user.enable_remote_guard == '0' || !this.users._user.enable_remote_guard ? false : true
    this.air_print = this.users._user.air_print;
    this.community_id = this.users._user.community_id;
    this.web_relays = await this.network.getWebRelays();
    const web_relay_id = this.users._user.selected_web_relay
      ? this.users._user.selected_web_relay
      : '';
    this.webRelay = this.web_relays.find((x) => x.id == web_relay_id);
    console.log(this.webRelay);
    this.loading = false;

    const camera_count = await this.network.getCameraCount();
    console.log(camera_count);
    var lpr = camera_count['lpr'];
    var non_lpr = camera_count['non-lpr'];
    var doorbird = camera_count['doorbird'];
    // this.setFlags();
    this.camerasAvailable = lpr + non_lpr > 0;
    const default_cam = await this.getDefaultCamera();
    if (default_cam) {
      this.default_cam = default_cam;
    }
    // const doorbird_cam = await this.getDoorbirdCamera();
    // if (doorbird_cam) {
    //   this.doorbird_camera = doorbird_cam;
    // }
    // const front_camera = await this.getFrontCamera();

    // if (front_camera) {
    //   this.front_camera = front_camera;
    // }

    // }
  }

  async getFrontCamera() {
    return new Promise(async (resolve) => {
      const front_camera_id = this.users._user.front_camera;

      const latlng = await this.geolocation.getCurrentLocationCoordinates();

      // const latlng = {
      //   lat: 24.920069922350827,
      //   long: 67.085827606491,
      // };

      latlng['type'] = 'front_camera';
      const data = await this.network.getCameras(latlng);

      if (!data || data.lenght == 0) {
        this.camerasAvailable = false;
      }

      let front_camera = data.find((x) => x.id == front_camera_id);

      resolve(front_camera);
    });
  }

  async getDefaultCamera() {
    return new Promise(async (resolve) => {
      const default_cam_id = this.users._user.default_camera;

      const latlng = await this.geolocation.getCurrentLocationCoordinates();

      // const latlng = {
      //   lat: 24.920069922350827,
      //   long: 67.085827606491,
      // };

      const data = await this.network.getCameras(latlng);

      if (!data || data.lenght == 0) {
        this.camerasAvailable = false;
        resolve(null);
        return;
      }

      let default_cam = data.find((x) => x.id == default_cam_id);

      resolve(default_cam);
    });
  }

  async getDoorbirdCamera() {
    return new Promise(async (resolve) => {
      var doorbird_cam_id = this.users._user.doorbird_cam_id;
      doorbird_cam_id = parseInt(doorbird_cam_id);

      // const latlng = await this.geolocation.getCurrentLocationCoordinates();

      // // const latlng = {
      // //   lat: 24.920069922350827,
      // //   long: 67.085827606491,
      // // };

      var obj = {
        type: 'doorbird',
      };

      const data = await this.network.getCameras(obj);

      if (!data || data.lenght == 0) {
        this.doorbirdCamerasAvailable = false;
        resolve(null);
        return;
      }

      let doorbird_cam = data.find((x) => x.id == doorbird_cam_id);

      resolve(doorbird_cam);
    });
  }

  async updateFlags() {
    localStorage.setItem('check_toggle', `${this.check_toggle}`);
    let cam_id;
    if (this.default_cam) {
      cam_id = this.default_cam.id.toString();
    } else {
      cam_id = this.users._user.default_camera;
    }
    const obj = {
      rover_mode: this.rover_mode == true ? '1' : '0',
      rover_type: this.rover_type,
      default_camera: cam_id,
      enable_remote_guard: this.enable_remote_guard == true ? '1' : '0',

      air_print: this.air_print.toString(),
      is_onboarded: this.is_onboarded == true ? '1' : '0',
      camera_id: this.front_camera ? this.front_camera.id.toString() : null,
      doorbird_cam_id: this.doorbird_camera
        ? this.doorbird_camera.id.toString()
        : null,
    };

    console.log(this.users._user.id);
    const update = await this.network.updateGuardProfile(obj);
    console.log(update);
    this.users.getUser();
    this.close();
  }

  async openCamerasModal(type = 'default') {
    const { data } = await this.modals.present(CamerasComponent, {
      type: type,
    });
    if (data.camera) {
      if (type == 'front_camera') {
        this.front_camera = data.camera;
      } else if (type == 'doorbird') {
        this.doorbird_camera = data.camera;
      } else {
        this.default_cam = data.camera;
      }
    }
  }

  updateDoorbird($event) {
    // console
    console.log($event.target.value, this.enableDoorbird);
    localStorage.setItem('enabledoorbird', `${this.enableDoorbird}`);
  }

  close() {
    this.modals.dismiss();
  }

  async openWebRelaysModal() {
    const { data } = await this.modals.present(WebRelayComponent, {
      web_relays: this.web_relays,
    });
    if (data.data !== 'A') {
      this.setWebRelay(data);
    }
  }

  setWebRelay(web_relay: any) {
    this.webRelay = web_relay;
    if (web_relay.id) {
      this.network.setGuardWebRelay(web_relay.id).then((res) => {
        if (res) {
          // this.utility.presentSuccessToast('Web Relay Set');
        }
      });
    }
  }

  // async resetOnboarding() {
  //   this.is_onboarded = false;
  //   this.updateFlags();
  //   await this.users.getUser();
  //   this.utility.showAlert(' Your Onboarding have been reset', ' Success !');
  // }
  // openRingCentralLogin() {
  //   this.modals.present(RingCentralLoginPage);
  // }
}
