import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-web-relay',
  templateUrl: './web-relay.component.html',
  styleUrls: ['./web-relay.component.scss'],
})
export class WebRelayComponent extends BasePage implements OnInit {
  web_relays: Array<any>;
  constructor(injector: Injector) {
    super(injector);
  }

  async ngOnInit() {}

  async getWebRelays() {
    this.web_relays = await this.network.getWebRelays();
  }

  closeModal(data: any) {
    this.modals.dismiss(data);
  }

  doRefresh() {
    this.getWebRelays();
  }
}
