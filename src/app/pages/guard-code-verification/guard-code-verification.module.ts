import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuardCodeVerificationPage } from './guard-code-verification';

@NgModule({
  declarations: [
    GuardCodeVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(GuardCodeVerificationPage),
  ],
})
export class GuardCodeVerificationPageModule {}
