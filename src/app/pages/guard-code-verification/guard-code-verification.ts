import { Component, Injector } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { BasePage } from '../base-page/base-page';
/**
 * Generated class for the GuardCodeVerificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'app-guard-code-verification',
  templateUrl: 'guard-code-verification.html',
  styleUrls: ['./guard-code-verification.scss'],
})
export class GuardCodeVerificationPage extends BasePage {

  aForm: FormGroup;
  submitAttempt = false;
  user: any;

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();


  }

  closeView() {
    this.modals.dismiss();
  }

  redirectToContactUsFOrm() {
    this.utility.openContactFormUrl();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CodeVerificationPage');
  }

  setupForm() {

    this.aForm = this.formBuilder.group({
      code: ['', Validators.compose([Validators.required])]
    });

  }

  verify() {

    const in_code = !this.aForm.controls.code.valid;

    if (in_code) {
      this.utility.presentFailureToast('Please Enter Verification Code');
      return;
    }

    this.submitAttempt = true;
    const formdata = this.aForm.value;

    this.network.verifyGuardLogin(formdata).then(res => {

      console.log(res);

      this.events.publish('user:process', res.user);
      this.modals.dismiss();
      // this.storage.set('accessToken', res.token).then( () => {
      //   this.navCtrl.setRoot(GuardCodeVerificationSuccessPage, {user_id: res.user_id}, {
      //     animate: true,
      //     direction: 'forward'
      //   });
      // });


    }, err => { });



  }

}
