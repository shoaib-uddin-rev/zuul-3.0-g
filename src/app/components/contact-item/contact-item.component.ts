import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EventsService } from 'src/app/services/events.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss'],
})
export class ContactItemComponent {

  _item: any;

  get item(): any{
    return this._item;
  }

  @Input() set item(value: any) {
    this._item = value;
  };

  @Input('singleSelection') singleSelection: boolean = false;

  @Output('addSelectedContact') addSelectedContact: EventEmitter<any> = new EventEmitter<any>()
  text: string;

  constructor(public events: EventsService) {

    this.events.subscribe('contact-list:check-update', this.updateCheckMark.bind(this))

  }

  updateCheckMark($event){
    var obj = $event;


    if(this.singleSelection){
      console.log(obj);
      this._item.is_assigned_temporary = false;
      if(obj['id'] == this._item.id){
        console.log(obj['id'], this._item.id, obj['includes'])
        this._item.is_assigned_temporary = obj['includes']
      }
    }


  }

}