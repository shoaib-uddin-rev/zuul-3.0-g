import {
  AfterViewInit,
  Component,
  Injector,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { BasePage } from '../../pages/base-page/base-page';
const countries = require('src/app/data/countries.json');
@Component({
  selector: 'app-country-code',
  templateUrl: './country-code.component.html',
  styleUrls: ['./country-code.component.scss'],
})
export class CountryCodeComponent
  extends BasePage
  implements OnInit, AfterViewInit
{
  @Input() dial_code;
  @Input() dc;
  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  item;
  offset = 0;
  search;

  countries: any[] = [];

  constructor(injector: Injector) {
    super(injector);

    this.dial_code = this.dc;
    this.countries = countries;
  }

  ngAfterViewInit(): void {
    var d: any = countries;
    this.countries = d.map((element) => {
      element['image'] =
        'assets/imgs/flags/' + element.code.toLowerCase() + '.png';

      return element;
    });

    // this.countries = d;
    // this.buffer_countries = d;
    console.log(this.countries);

    // this.getCountriesList(null, 0, false, false);
  }

  ngOnInit() {}

  // setFocusOnSearch(){
  //   var self = this;
  //   this.search_on = true;
  //   setTimeout( () => {
  //     self.searchbar.setFocus();
  //   }, 500);

  // }

  onSelectCountry(item) {
    this.modals.dismiss(item);
  }

  closeModal(data) {
    this.modals.dismiss(data);
  }
}
