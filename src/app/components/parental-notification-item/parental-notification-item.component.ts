import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-parental-notification-item',
  templateUrl: './parental-notification-item.component.html',
  styleUrls: ['./parental-notification-item.component.scss'],
})
export class ParentalNotificationItemComponent implements OnInit {

  @Input() itm: any;
  @Output('markasread') markasread: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {}

  

}
