import { NgModule } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { ParentalNotificationItemComponent } from './parental-notification-item.component';

@NgModule({
	declarations: [
		ParentalNotificationItemComponent
	],
	imports: [
    IonicModule,
	],
	exports: [
		ParentalNotificationItemComponent
	]
})
export class ParentalNotificationItemComponentModule {}
